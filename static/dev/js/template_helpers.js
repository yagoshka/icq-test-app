Handlebars.registerHelper('page_wrapper', function(ctx, opts){
    Handlebars.partials.page_wrapper({});
});
Handlebars.registerHelper('page_head', function(ctx, opts){});
Handlebars.registerHelper('page_content', function(ctx, opts){});

Handlebars.registerHelper('getDuration', function(ctx, options) {
    var sec_num = parseInt(ctx, 10);
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = (hours > 0 ?hours+':':'')+minutes+':'+seconds;

    return time;
});

Handlebars.registerHelper('plural', function(count, one, few, many) {
    var pluralized = one, div = count % 10;

    if (many === undefined || typeof many !== 'string') {
        many = few;
    }

    if (div >= 2 && div <= 5) {
        pluralized = few;
    }

    if (count % 10 >= 5) {
        pluralized = many;
    }

    return pluralized;
});

Handlebars.registerHelper('format_number', function(number) {
    number = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');

    return number;
});

Handlebars.registerHelper('message', function(ctx, options) {
    return Handlebars.partials.message(ctx.hash);
});