this["JST"] = this["JST"] || {};

Handlebars.registerPartial("api_method_invoker", Handlebars.template({"1":function(depth0,helpers,partials,data) {
  return "data-cb=\"true\"";
  },"3":function(depth0,helpers,partials,data) {
  return "data-user-input=\"true\"";
  },"5":function(depth0,helpers,partials,data) {
  return "data-native=\"1\"";
  },"7":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", buffer = "\n            ";
  stack1 = ((helper = helpers.method_desc || (depth0 && depth0.method_desc)),(typeof helper === functionType ? helper.call(depth0, {"name":"method_desc","hash":{},"data":data}) : helper));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer + "\n        ";
},"9":function(depth0,helpers,partials,data) {
  var helper, functionType="function", escapeExpression=this.escapeExpression;
  return "\n            "
    + escapeExpression(((helper = helpers.method_name || (depth0 && depth0.method_name)),(typeof helper === functionType ? helper.call(depth0, {"name":"method_name","hash":{},"data":data}) : helper)))
    + "\n        ";
},"11":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", buffer = "\n    <script type=\"text/plain\" class=\"cb\">\n        ";
  stack1 = ((helper = helpers.method_cb || (depth0 && depth0.method_cb)),(typeof helper === functionType ? helper.call(depth0, {"name":"method_cb","hash":{},"data":data}) : helper));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer + "\n    </script>\n    ";
},"compiler":[5,">= 2.0.0"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", escapeExpression=this.escapeExpression, buffer = "<div class=\"api-invoker api-status-"
    + escapeExpression(((helper = helpers.status || (depth0 && depth0.status)),(typeof helper === functionType ? helper.call(depth0, {"name":"status","hash":{},"data":data}) : helper)))
    + "\" ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.has_cb), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.user_input), {"name":"if","hash":{},"fn":this.program(3, data),"inverse":this.noop,"data":data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " data-action=\""
    + escapeExpression(((helper = helpers.method_name || (depth0 && depth0.method_name)),(typeof helper === functionType ? helper.call(depth0, {"name":"method_name","hash":{},"data":data}) : helper)))
    + "\" data-params=\""
    + escapeExpression(((helper = helpers.method_params || (depth0 && depth0.method_params)),(typeof helper === functionType ? helper.call(depth0, {"name":"method_params","hash":{},"data":data}) : helper)))
    + "\" ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0['native']), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.noop,"data":data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " data-response-mock=\""
    + escapeExpression(((helper = helpers.response_data_mock || (depth0 && depth0.response_data_mock)),(typeof helper === functionType ? helper.call(depth0, {"name":"response_data_mock","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"button app-button\">\n        ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.method_desc), {"name":"if","hash":{},"fn":this.program(7, data),"inverse":this.program(9, data),"data":data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n    </div>\n    <pre class=\"result\"></pre>\n    ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.method_cb), {"name":"if","hash":{},"fn":this.program(11, data),"inverse":this.noop,"data":data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer + "\n</div>";
},"useData":true}));

Handlebars.registerPartial("main_page", Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\n            ";
  stack1 = this.invokePartial(partials._api_method_invoker, '_api_method_invoker', depth0, undefined, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer + "\n        ";
},"compiler":[5,">= 2.0.0"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<div class=\"app-page app-page-main\" data-page=\"main\" data-page-title=\"\" style=\"display: block\">\n    <div id=\"profile\"></div>\n    <div id=\"timer\"></div>\n    <div id=\"local-storage\"></div>\n    <div id=\"push-data\" class=\"custom-button\" style=\"display: none\">\n        <div class=\"api-status-done\" >\n            <div class=\"button app-button\">\n                PUSH NOTIFICATION DATA\n            </div>\n            <pre class=\"result\" style=\"display: block\"></pre>\n        </div>\n    </div>\n    <div id=\"get-params\" class=\"custom-button\">\n        <div class=\"api-status-done\" >\n            <div class=\"button app-button\">\n                Show GET-params\n            </div>\n            <pre class=\"result\"></pre>\n        </div>\n    </div>\n\n    <!--<div id=\"show-mailru\" class=\"custom-button\">-->\n        <!--<div class=\"api-status-done\" >-->\n            <!--<div class=\"button app-button\">-->\n                <!--Show MAILRU methods-->\n            <!--</div>-->\n            <!--<pre class=\"result\"></pre>-->\n        <!--</div>-->\n    <!--</div>-->\n    <!---->\n    <div class=\"custom-container\">\n        <input type=\"file\" />\n        <pre class=\"result\"></pre>\n    </div>\n\n    <div class=\"app-page-content\">\n        ";
  stack1 = helpers.each.call(depth0, (depth0 && depth0.methods), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer + "\n    </div>\n\n    <div class=\"custom-button\" id=\"console\">\n        <div class=\"api-status-done\" >\n            <div class=\"button app-button\">\n                console\n            </div>\n        </div>\n\n    </div>\n\n    <div class=\"custom-button\" id=\"console\">\n        <div class=\"api-status-done\">\n            <a class=\"button app-button\" href=\"market://search?q=name:com.icq.mobile.client\">1. store scheme ICQ in Play Store</a>\n            <a class=\"button app-button\" href=\"intent://scan/#Intent;scheme=icq;package=com.icq.mobile.client;end\">2. intent ICQ in Play Store</a>\n            <a class=\"button app-button\" href=\"https://play.google.com/store/apps/details?id=com.icq.mobile.client\">3. direct link ICQ in Play Store</a>\n        </div>\n\n    </div>\n\n\n\n    <div id=\"friends-list\">\n        <h3>\n            Friends with APPS support\n        </h3>\n    </div>\n\n    <div id=\"friend-canvas\">\n\n    </div>\n</div>\n";
},"usePartial":true,"useData":true}));

Handlebars.registerPartial("scripts", Handlebars.template({"compiler":[5,">= 2.0.0"],"main":function(depth0,helpers,partials,data) {
  return "<script src=\"js/lib.js\" defer></script>\n<script src=\"js/all.js\" defer></script>";
  },"useData":true}));

Handlebars.registerPartial("search_for_app_page", Handlebars.template({"compiler":[5,">= 2.0.0"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"app-page app-page-search\" data-page=\"search\" data-page-title=\"\">\n    <div id=\"profile\"></div>\n    <div class=\"app-page-content\">\n        <div class=\"search-field\">\n            <form>\n                <div class=\"icon\"></div>\n                <input type=\"search\" class=\"search-field__input\" required=\"\"  placeholder=\"Search\" />\n                <button type=\"submit\" class=\"search-field__submit\"></button>\n            </form>\n        </div>\n    </div>\n</div>\n";
  },"useData":true}));

Handlebars.registerPartial("user_profile", Handlebars.template({"compiler":[5,">= 2.0.0"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", escapeExpression=this.escapeExpression;
  return "<div class=\"user-profile\">\n    <div class=\"user-profile__avatar\">\n        <img src=\""
    + escapeExpression(((helper = helpers.avatar || (depth0 && depth0.avatar)),(typeof helper === functionType ? helper.call(depth0, {"name":"avatar","hash":{},"data":data}) : helper)))
    + "\" alt=\""
    + escapeExpression(((helper = helpers.first_name || (depth0 && depth0.first_name)),(typeof helper === functionType ? helper.call(depth0, {"name":"first_name","hash":{},"data":data}) : helper)))
    + " "
    + escapeExpression(((helper = helpers.last_name || (depth0 && depth0.last_name)),(typeof helper === functionType ? helper.call(depth0, {"name":"last_name","hash":{},"data":data}) : helper)))
    + "\"/>\n    </div>\n    <div class=\"user-profile__name\">\n        "
    + escapeExpression(((helper = helpers.first_name || (depth0 && depth0.first_name)),(typeof helper === functionType ? helper.call(depth0, {"name":"first_name","hash":{},"data":data}) : helper)))
    + " "
    + escapeExpression(((helper = helpers.last_name || (depth0 && depth0.last_name)),(typeof helper === functionType ? helper.call(depth0, {"name":"last_name","hash":{},"data":data}) : helper)))
    + "\n    </div>\n\n    <div class=\"user-profile__age\">\n        "
    + escapeExpression(((helper = helpers.birthday || (depth0 && depth0.birthday)),(typeof helper === functionType ? helper.call(depth0, {"name":"birthday","hash":{},"data":data}) : helper)))
    + "\n    </div>\n    <!--<div class=\"user-profile__friends_count\">-->\n        <!--"
    + escapeExpression(((helper = helpers.contacts_count || (depth0 && depth0.contacts_count)),(typeof helper === functionType ? helper.call(depth0, {"name":"contacts_count","hash":{},"data":data}) : helper)))
    + "-->\n    <!--</div>-->\n</div>";
},"useData":true}));

this["JST"]["templates/layout.handlebars"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  return "manifest=\"/manifest.appcache\"";
  },"compiler":[5,">= 2.0.0"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "<!DOCTYPE html>\n<html ";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 && depth0.env)),stack1 == null || stack1 === false ? stack1 : stack1.prod), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">\n    <head>\n        <title>ICQ TEST APP</title>\n        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />\n        <link rel=\"stylesheet\" href=\"css/app/app.css\"/>\n        <link rel=\"stylesheet\" href=\"css/main.css\"/>\n        <script>\n            window.onerror = function(err) {\n                alert(err)\n            }\n        </script>\n    </head>\n    <body>\n        <div class=\"app-page-header-container\">\n            <div class=\"app-page-header\">\n                <div class=\"app-button left app-page-back-button\" data-back=\"true\">\n                    <span class=\"icon\"></span>\n                    <span class=\"caption\">Back</span>\n                </div>\n                <div class=\"app-page-title\">\n                    <span class=\"caption\">Test app</span>\n                </div>\n                <div class=\"app-button right app-open-dev\" id=\"open-test-app\">\n                    <span class=\"icon\"></span>\n                    <span class=\"caption\">test</span>\n                </div>\n            </div>\n\n        </div>\n\n        ";
  stack1 = this.invokePartial(partials._main_page, '_main_page', depth0, undefined, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n        ";
  stack1 = this.invokePartial(partials._search_for_app_page, '_search_for_app_page', depth0, undefined, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n        ";
  stack1 = this.invokePartial(partials._scripts, '_scripts', depth0, undefined, helpers, partials, data);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer + "\n    </body>\n</html>";
},"usePartial":true,"useData":true});