/*! Youtube app - v - 2015-01-29
 */var Zepto=function(){function L(t){return null==t?String(t):j[T.call(t)]||"object"}function Z(t){return"function"==L(t)}function $(t){return null!=t&&t==t.window}function _(t){return null!=t&&t.nodeType==t.DOCUMENT_NODE}function D(t){return"object"==L(t)}function R(t){return D(t)&&!$(t)&&Object.getPrototypeOf(t)==Object.prototype}function M(t){return"number"==typeof t.length}function k(t){return s.call(t,function(t){return null!=t})}function z(t){return t.length>0?n.fn.concat.apply([],t):t}function F(t){return t.replace(/::/g,"/").replace(/([A-Z]+)([A-Z][a-z])/g,"$1_$2").replace(/([a-z\d])([A-Z])/g,"$1_$2").replace(/_/g,"-").toLowerCase()}function q(t){return t in f?f[t]:f[t]=new RegExp("(^|\\s)"+t+"(\\s|$)")}function H(t,e){return"number"!=typeof e||c[F(t)]?e:e+"px"}function I(t){var e,n;return u[t]||(e=a.createElement(t),a.body.appendChild(e),n=getComputedStyle(e,"").getPropertyValue("display"),e.parentNode.removeChild(e),"none"==n&&(n="block"),u[t]=n),u[t]}function V(t){return"children"in t?o.call(t.children):n.map(t.childNodes,function(t){return 1==t.nodeType?t:void 0})}function U(n,i,r){for(e in i)r&&(R(i[e])||A(i[e]))?(R(i[e])&&!R(n[e])&&(n[e]={}),A(i[e])&&!A(n[e])&&(n[e]=[]),U(n[e],i[e],r)):i[e]!==t&&(n[e]=i[e])}function B(t,e){return null==e?n(t):n(t).filter(e)}function J(t,e,n,i){return Z(e)?e.call(t,n,i):e}function X(t,e,n){null==n?t.removeAttribute(e):t.setAttribute(e,n)}function W(e,n){var i=e.className,r=i&&i.baseVal!==t;return n===t?r?i.baseVal:i:void(r?i.baseVal=n:e.className=n)}function Y(t){var e;try{return t?"true"==t||("false"==t?!1:"null"==t?null:/^0/.test(t)||isNaN(e=Number(t))?/^[\[\{]/.test(t)?n.parseJSON(t):t:e):t}catch(i){return t}}function G(t,e){e(t);for(var n in t.childNodes)G(t.childNodes[n],e)}var t,e,n,i,C,N,r=[],o=r.slice,s=r.filter,a=window.document,u={},f={},c={"column-count":1,columns:1,"font-weight":1,"line-height":1,opacity:1,"z-index":1,zoom:1},l=/^\s*<(\w+|!)[^>]*>/,h=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,p=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,d=/^(?:body|html)$/i,m=/([A-Z])/g,g=["val","css","html","text","data","width","height","offset"],v=["after","prepend","before","append"],y=a.createElement("table"),x=a.createElement("tr"),b={tr:a.createElement("tbody"),tbody:y,thead:y,tfoot:y,td:x,th:x,"*":a.createElement("div")},w=/complete|loaded|interactive/,E=/^[\w-]*$/,j={},T=j.toString,S={},O=a.createElement("div"),P={tabindex:"tabIndex",readonly:"readOnly","for":"htmlFor","class":"className",maxlength:"maxLength",cellspacing:"cellSpacing",cellpadding:"cellPadding",rowspan:"rowSpan",colspan:"colSpan",usemap:"useMap",frameborder:"frameBorder",contenteditable:"contentEditable"},A=Array.isArray||function(t){return t instanceof Array};return S.matches=function(t,e){if(!e||!t||1!==t.nodeType)return!1;var n=t.webkitMatchesSelector||t.mozMatchesSelector||t.oMatchesSelector||t.matchesSelector;if(n)return n.call(t,e);var i,r=t.parentNode,o=!r;return o&&(r=O).appendChild(t),i=~S.qsa(r,e).indexOf(t),o&&O.removeChild(t),i},C=function(t){return t.replace(/-+(.)?/g,function(t,e){return e?e.toUpperCase():""})},N=function(t){return s.call(t,function(e,n){return t.indexOf(e)==n})},S.fragment=function(e,i,r){var s,u,f;return h.test(e)&&(s=n(a.createElement(RegExp.$1))),s||(e.replace&&(e=e.replace(p,"<$1></$2>")),i===t&&(i=l.test(e)&&RegExp.$1),i in b||(i="*"),f=b[i],f.innerHTML=""+e,s=n.each(o.call(f.childNodes),function(){f.removeChild(this)})),R(r)&&(u=n(s),n.each(r,function(t,e){g.indexOf(t)>-1?u[t](e):u.attr(t,e)})),s},S.Z=function(t,e){return t=t||[],t.__proto__=n.fn,t.selector=e||"",t},S.isZ=function(t){return t instanceof S.Z},S.init=function(e,i){var r;if(!e)return S.Z();if("string"==typeof e)if(e=e.trim(),"<"==e[0]&&l.test(e))r=S.fragment(e,RegExp.$1,i),e=null;else{if(i!==t)return n(i).find(e);r=S.qsa(a,e)}else{if(Z(e))return n(a).ready(e);if(S.isZ(e))return e;if(A(e))r=k(e);else if(D(e))r=[e],e=null;else if(l.test(e))r=S.fragment(e.trim(),RegExp.$1,i),e=null;else{if(i!==t)return n(i).find(e);r=S.qsa(a,e)}}return S.Z(r,e)},n=function(t,e){return S.init(t,e)},n.extend=function(t){var e,n=o.call(arguments,1);return"boolean"==typeof t&&(e=t,t=n.shift()),n.forEach(function(n){U(t,n,e)}),t},S.qsa=function(t,e){var n,i="#"==e[0],r=!i&&"."==e[0],s=i||r?e.slice(1):e,a=E.test(s);return _(t)&&a&&i?(n=t.getElementById(s))?[n]:[]:1!==t.nodeType&&9!==t.nodeType?[]:o.call(a&&!i?r?t.getElementsByClassName(s):t.getElementsByTagName(e):t.querySelectorAll(e))},n.contains=function(t,e){return t!==e&&t.contains(e)},n.type=L,n.isFunction=Z,n.isWindow=$,n.isArray=A,n.isPlainObject=R,n.isEmptyObject=function(t){var e;for(e in t)return!1;return!0},n.inArray=function(t,e,n){return r.indexOf.call(e,t,n)},n.camelCase=C,n.trim=function(t){return null==t?"":String.prototype.trim.call(t)},n.uuid=0,n.support={},n.expr={},n.map=function(t,e){var n,r,o,i=[];if(M(t))for(r=0;r<t.length;r++)n=e(t[r],r),null!=n&&i.push(n);else for(o in t)n=e(t[o],o),null!=n&&i.push(n);return z(i)},n.each=function(t,e){var n,i;if(M(t)){for(n=0;n<t.length;n++)if(e.call(t[n],n,t[n])===!1)return t}else for(i in t)if(e.call(t[i],i,t[i])===!1)return t;return t},n.grep=function(t,e){return s.call(t,e)},window.JSON&&(n.parseJSON=JSON.parse),n.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),function(t,e){j["[object "+e+"]"]=e.toLowerCase()}),n.fn={forEach:r.forEach,reduce:r.reduce,push:r.push,sort:r.sort,indexOf:r.indexOf,concat:r.concat,map:function(t){return n(n.map(this,function(e,n){return t.call(e,n,e)}))},slice:function(){return n(o.apply(this,arguments))},ready:function(t){return w.test(a.readyState)&&a.body?t(n):a.addEventListener("DOMContentLoaded",function(){t(n)},!1),this},get:function(e){return e===t?o.call(this):this[e>=0?e:e+this.length]},toArray:function(){return this.get()},size:function(){return this.length},remove:function(){return this.each(function(){null!=this.parentNode&&this.parentNode.removeChild(this)})},each:function(t){return r.every.call(this,function(e,n){return t.call(e,n,e)!==!1}),this},filter:function(t){return Z(t)?this.not(this.not(t)):n(s.call(this,function(e){return S.matches(e,t)}))},add:function(t,e){return n(N(this.concat(n(t,e))))},is:function(t){return this.length>0&&S.matches(this[0],t)},not:function(e){var i=[];if(Z(e)&&e.call!==t)this.each(function(t){e.call(this,t)||i.push(this)});else{var r="string"==typeof e?this.filter(e):M(e)&&Z(e.item)?o.call(e):n(e);this.forEach(function(t){r.indexOf(t)<0&&i.push(t)})}return n(i)},has:function(t){return this.filter(function(){return D(t)?n.contains(this,t):n(this).find(t).size()})},eq:function(t){return-1===t?this.slice(t):this.slice(t,+t+1)},first:function(){var t=this[0];return t&&!D(t)?t:n(t)},last:function(){var t=this[this.length-1];return t&&!D(t)?t:n(t)},find:function(t){var e,i=this;return e="object"==typeof t?n(t).filter(function(){var t=this;return r.some.call(i,function(e){return n.contains(e,t)})}):1==this.length?n(S.qsa(this[0],t)):this.map(function(){return S.qsa(this,t)})},closest:function(t,e){var i=this[0],r=!1;for("object"==typeof t&&(r=n(t));i&&!(r?r.indexOf(i)>=0:S.matches(i,t));)i=i!==e&&!_(i)&&i.parentNode;return n(i)},parents:function(t){for(var e=[],i=this;i.length>0;)i=n.map(i,function(t){return(t=t.parentNode)&&!_(t)&&e.indexOf(t)<0?(e.push(t),t):void 0});return B(e,t)},parent:function(t){return B(N(this.pluck("parentNode")),t)},children:function(t){return B(this.map(function(){return V(this)}),t)},contents:function(){return this.map(function(){return o.call(this.childNodes)})},siblings:function(t){return B(this.map(function(t,e){return s.call(V(e.parentNode),function(t){return t!==e})}),t)},empty:function(){return this.each(function(){this.innerHTML=""})},pluck:function(t){return n.map(this,function(e){return e[t]})},show:function(){return this.each(function(){"none"==this.style.display&&(this.style.display=""),"none"==getComputedStyle(this,"").getPropertyValue("display")&&(this.style.display=I(this.nodeName))})},replaceWith:function(t){return this.before(t).remove()},wrap:function(t){var e=Z(t);if(this[0]&&!e)var i=n(t).get(0),r=i.parentNode||this.length>1;return this.each(function(o){n(this).wrapAll(e?t.call(this,o):r?i.cloneNode(!0):i)})},wrapAll:function(t){if(this[0]){n(this[0]).before(t=n(t));for(var e;(e=t.children()).length;)t=e.first();n(t).append(this)}return this},wrapInner:function(t){var e=Z(t);return this.each(function(i){var r=n(this),o=r.contents(),s=e?t.call(this,i):t;o.length?o.wrapAll(s):r.append(s)})},unwrap:function(){return this.parent().each(function(){n(this).replaceWith(n(this).children())}),this},clone:function(){return this.map(function(){return this.cloneNode(!0)})},hide:function(){return this.css("display","none")},toggle:function(e){return this.each(function(){var i=n(this);(e===t?"none"==i.css("display"):e)?i.show():i.hide()})},prev:function(t){return n(this.pluck("previousElementSibling")).filter(t||"*")},next:function(t){return n(this.pluck("nextElementSibling")).filter(t||"*")},html:function(t){return 0===arguments.length?this.length>0?this[0].innerHTML:null:this.each(function(e){var i=this.innerHTML;n(this).empty().append(J(this,t,e,i))})},text:function(e){return 0===arguments.length?this.length>0?this[0].textContent:null:this.each(function(){this.textContent=e===t?"":""+e})},attr:function(n,i){var r;return"string"==typeof n&&i===t?0==this.length||1!==this[0].nodeType?t:"value"==n&&"INPUT"==this[0].nodeName?this.val():!(r=this[0].getAttribute(n))&&n in this[0]?this[0][n]:r:this.each(function(t){if(1===this.nodeType)if(D(n))for(e in n)X(this,e,n[e]);else X(this,n,J(this,i,t,this.getAttribute(n)))})},removeAttr:function(t){return this.each(function(){1===this.nodeType&&X(this,t)})},prop:function(e,n){return e=P[e]||e,n===t?this[0]&&this[0][e]:this.each(function(t){this[e]=J(this,n,t,this[e])})},data:function(e,n){var i=this.attr("data-"+e.replace(m,"-$1").toLowerCase(),n);return null!==i?Y(i):t},val:function(t){return 0===arguments.length?this[0]&&(this[0].multiple?n(this[0]).find("option").filter(function(){return this.selected}).pluck("value"):this[0].value):this.each(function(e){this.value=J(this,t,e,this.value)})},offset:function(t){if(t)return this.each(function(e){var i=n(this),r=J(this,t,e,i.offset()),o=i.offsetParent().offset(),s={top:r.top-o.top,left:r.left-o.left};"static"==i.css("position")&&(s.position="relative"),i.css(s)});if(0==this.length)return null;var e=this[0].getBoundingClientRect();return{left:e.left+window.pageXOffset,top:e.top+window.pageYOffset,width:Math.round(e.width),height:Math.round(e.height)}},css:function(t,i){if(arguments.length<2){var r=this[0],o=getComputedStyle(r,"");if(!r)return;if("string"==typeof t)return r.style[C(t)]||o.getPropertyValue(t);if(A(t)){var s={};return n.each(A(t)?t:[t],function(t,e){s[e]=r.style[C(e)]||o.getPropertyValue(e)}),s}}var a="";if("string"==L(t))i||0===i?a=F(t)+":"+H(t,i):this.each(function(){this.style.removeProperty(F(t))});else for(e in t)t[e]||0===t[e]?a+=F(e)+":"+H(e,t[e])+";":this.each(function(){this.style.removeProperty(F(e))});return this.each(function(){this.style.cssText+=";"+a})},index:function(t){return t?this.indexOf(n(t)[0]):this.parent().children().indexOf(this[0])},hasClass:function(t){return t?r.some.call(this,function(t){return this.test(W(t))},q(t)):!1},addClass:function(t){return t?this.each(function(e){i=[];var r=W(this),o=J(this,t,e,r);o.split(/\s+/g).forEach(function(t){n(this).hasClass(t)||i.push(t)},this),i.length&&W(this,r+(r?" ":"")+i.join(" "))}):this},removeClass:function(e){return this.each(function(n){return e===t?W(this,""):(i=W(this),J(this,e,n,i).split(/\s+/g).forEach(function(t){i=i.replace(q(t)," ")}),void W(this,i.trim()))})},toggleClass:function(e,i){return e?this.each(function(r){var o=n(this),s=J(this,e,r,W(this));s.split(/\s+/g).forEach(function(e){(i===t?!o.hasClass(e):i)?o.addClass(e):o.removeClass(e)})}):this},scrollTop:function(e){if(this.length){var n="scrollTop"in this[0];return e===t?n?this[0].scrollTop:this[0].pageYOffset:this.each(n?function(){this.scrollTop=e}:function(){this.scrollTo(this.scrollX,e)})}},scrollLeft:function(e){if(this.length){var n="scrollLeft"in this[0];return e===t?n?this[0].scrollLeft:this[0].pageXOffset:this.each(n?function(){this.scrollLeft=e}:function(){this.scrollTo(e,this.scrollY)})}},position:function(){if(this.length){var t=this[0],e=this.offsetParent(),i=this.offset(),r=d.test(e[0].nodeName)?{top:0,left:0}:e.offset();return i.top-=parseFloat(n(t).css("margin-top"))||0,i.left-=parseFloat(n(t).css("margin-left"))||0,r.top+=parseFloat(n(e[0]).css("border-top-width"))||0,r.left+=parseFloat(n(e[0]).css("border-left-width"))||0,{top:i.top-r.top,left:i.left-r.left}}},offsetParent:function(){return this.map(function(){for(var t=this.offsetParent||a.body;t&&!d.test(t.nodeName)&&"static"==n(t).css("position");)t=t.offsetParent;return t})}},n.fn.detach=n.fn.remove,["width","height"].forEach(function(e){var i=e.replace(/./,function(t){return t[0].toUpperCase()});n.fn[e]=function(r){var o,s=this[0];return r===t?$(s)?s["inner"+i]:_(s)?s.documentElement["scroll"+i]:(o=this.offset())&&o[e]:this.each(function(t){s=n(this),s.css(e,J(this,r,t,s[e]()))})}}),v.forEach(function(t,e){var i=e%2;n.fn[t]=function(){var t,o,r=n.map(arguments,function(e){return t=L(e),"object"==t||"array"==t||null==e?e:S.fragment(e)}),s=this.length>1;return r.length<1?this:this.each(function(t,a){o=i?a:a.parentNode,a=0==e?a.nextSibling:1==e?a.firstChild:2==e?a:null,r.forEach(function(t){if(s)t=t.cloneNode(!0);else if(!o)return n(t).remove();G(o.insertBefore(t,a),function(t){null==t.nodeName||"SCRIPT"!==t.nodeName.toUpperCase()||t.type&&"text/javascript"!==t.type||t.src||window.eval.call(window,t.innerHTML)})})})},n.fn[i?t+"To":"insert"+(e?"Before":"After")]=function(e){return n(e)[t](this),this}}),S.Z.prototype=n.fn,S.uniq=N,S.deserializeValue=Y,n.zepto=S,n}();window.Zepto=Zepto,void 0===window.$&&(window.$=Zepto),function(t){function l(t){return t._zid||(t._zid=e++)}function h(t,e,n,i){if(e=p(e),e.ns)var r=d(e.ns);return(s[l(t)]||[]).filter(function(t){return!(!t||e.e&&t.e!=e.e||e.ns&&!r.test(t.ns)||n&&l(t.fn)!==l(n)||i&&t.sel!=i)})}function p(t){var e=(""+t).split(".");return{e:e[0],ns:e.slice(1).sort().join(" ")}}function d(t){return new RegExp("(?:^| )"+t.replace(" "," .* ?")+"(?: |$)")}function m(t,e){return t.del&&!u&&t.e in f||!!e}function g(t){return c[t]||u&&f[t]||t}function v(e,i,r,o,a,u,f){var h=l(e),d=s[h]||(s[h]=[]);i.split(/\s/).forEach(function(i){if("ready"==i)return t(document).ready(r);var s=p(i);s.fn=r,s.sel=a,s.e in c&&(r=function(e){var n=e.relatedTarget;return!n||n!==this&&!t.contains(this,n)?s.fn.apply(this,arguments):void 0}),s.del=u;var l=u||r;s.proxy=function(t){if(t=j(t),!t.isImmediatePropagationStopped()){t.data=o;var i=l.apply(e,t._args==n?[t]:[t].concat(t._args));return i===!1&&(t.preventDefault(),t.stopPropagation()),i}},s.i=d.length,d.push(s),"addEventListener"in e&&e.addEventListener(g(s.e),s.proxy,m(s,f))})}function y(t,e,n,i,r){var o=l(t);(e||"").split(/\s/).forEach(function(e){h(t,e,n,i).forEach(function(e){delete s[o][e.i],"removeEventListener"in t&&t.removeEventListener(g(e.e),e.proxy,m(e,r))})})}function j(e,i){return(i||!e.isDefaultPrevented)&&(i||(i=e),t.each(E,function(t,n){var r=i[t];e[t]=function(){return this[n]=x,r&&r.apply(i,arguments)},e[n]=b}),(i.defaultPrevented!==n?i.defaultPrevented:"returnValue"in i?i.returnValue===!1:i.getPreventDefault&&i.getPreventDefault())&&(e.isDefaultPrevented=x)),e}function T(t){var e,i={originalEvent:t};for(e in t)w.test(e)||t[e]===n||(i[e]=t[e]);return j(i,t)}var n,e=1,i=Array.prototype.slice,r=t.isFunction,o=function(t){return"string"==typeof t},s={},a={},u="onfocusin"in window,f={focus:"focusin",blur:"focusout"},c={mouseenter:"mouseover",mouseleave:"mouseout"};a.click=a.mousedown=a.mouseup=a.mousemove="MouseEvents",t.event={add:v,remove:y},t.proxy=function(e,n){if(r(e)){var i=function(){return e.apply(n,arguments)};return i._zid=l(e),i}if(o(n))return t.proxy(e[n],e);throw new TypeError("expected function")},t.fn.bind=function(t,e,n){return this.on(t,e,n)},t.fn.unbind=function(t,e){return this.off(t,e)},t.fn.one=function(t,e,n,i){return this.on(t,e,n,i,1)};var x=function(){return!0},b=function(){return!1},w=/^([A-Z]|returnValue$|layer[XY]$)/,E={preventDefault:"isDefaultPrevented",stopImmediatePropagation:"isImmediatePropagationStopped",stopPropagation:"isPropagationStopped"};t.fn.delegate=function(t,e,n){return this.on(e,t,n)},t.fn.undelegate=function(t,e,n){return this.off(e,t,n)},t.fn.live=function(e,n){return t(document.body).delegate(this.selector,e,n),this},t.fn.die=function(e,n){return t(document.body).undelegate(this.selector,e,n),this},t.fn.on=function(e,s,a,u,f){var c,l,h=this;return e&&!o(e)?(t.each(e,function(t,e){h.on(t,s,a,e,f)}),h):(o(s)||r(u)||u===!1||(u=a,a=s,s=n),(r(a)||a===!1)&&(u=a,a=n),u===!1&&(u=b),h.each(function(n,r){f&&(c=function(t){return y(r,t.type,u),u.apply(this,arguments)}),s&&(l=function(e){var n,o=t(e.target).closest(s,r).get(0);return o&&o!==r?(n=t.extend(T(e),{currentTarget:o,liveFired:r}),(c||u).apply(o,[n].concat(i.call(arguments,1)))):void 0}),v(r,e,u,a,s,l||c)}))},t.fn.off=function(e,i,s){var a=this;return e&&!o(e)?(t.each(e,function(t,e){a.off(t,i,e)}),a):(o(i)||r(s)||s===!1||(s=i,i=n),s===!1&&(s=b),a.each(function(){y(this,e,s,i)}))},t.fn.trigger=function(e,n){return e=o(e)||t.isPlainObject(e)?t.Event(e):j(e),e._args=n,this.each(function(){"dispatchEvent"in this?this.dispatchEvent(e):t(this).triggerHandler(e,n)})},t.fn.triggerHandler=function(e,n){var i,r;return this.each(function(s,a){i=T(o(e)?t.Event(e):e),i._args=n,i.target=a,t.each(h(a,e.type||e),function(t,e){return r=e.proxy(i),i.isImmediatePropagationStopped()?!1:void 0})}),r},"focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select keydown keypress keyup error".split(" ").forEach(function(e){t.fn[e]=function(t){return t?this.bind(e,t):this.trigger(e)}}),["focus","blur"].forEach(function(e){t.fn[e]=function(t){return t?this.bind(e,t):this.each(function(){try{this[e]()}catch(t){}}),this}}),t.Event=function(t,e){o(t)||(e=t,t=e.type);var n=document.createEvent(a[t]||"Events"),i=!0;if(e)for(var r in e)"bubbles"==r?i=!!e[r]:n[r]=e[r];return n.initEvent(t,i,!0),j(n)}}(Zepto),function(t){function l(e,n,i){var r=t.Event(n);return t(e).trigger(r,i),!r.isDefaultPrevented()}function h(t,e,i,r){return t.global?l(e||n,i,r):void 0}function p(e){e.global&&0===t.active++&&h(e,null,"ajaxStart")}function d(e){e.global&&!--t.active&&h(e,null,"ajaxStop")}function m(t,e){var n=e.context;return e.beforeSend.call(n,t,e)===!1||h(e,n,"ajaxBeforeSend",[t,e])===!1?!1:void h(e,n,"ajaxSend",[t,e])}function g(t,e,n,i){var r=n.context,o="success";n.success.call(r,t,o,e),i&&i.resolveWith(r,[t,o,e]),h(n,r,"ajaxSuccess",[e,n,t]),y(o,e,n)}function v(t,e,n,i,r){var o=i.context;i.error.call(o,n,e,t),r&&r.rejectWith(o,[n,e,t]),h(i,o,"ajaxError",[n,i,t||e]),y(e,n,i)}function y(t,e,n){var i=n.context;n.complete.call(i,e,t),h(n,i,"ajaxComplete",[e,n]),d(n)}function x(){}function b(t){return t&&(t=t.split(";",2)[0]),t&&(t==f?"html":t==u?"json":s.test(t)?"script":a.test(t)&&"xml")||"text"}function w(t,e){return""==e?t:(t+"&"+e).replace(/[&?]{1,2}/,"?")}function E(e){e.processData&&e.data&&"string"!=t.type(e.data)&&(e.data=t.param(e.data,e.traditional)),!e.data||e.type&&"GET"!=e.type.toUpperCase()||(e.url=w(e.url,e.data),e.data=void 0)}function j(e,n,i,r){return t.isFunction(n)&&(r=i,i=n,n=void 0),t.isFunction(i)||(r=i,i=void 0),{url:e,data:n,success:i,dataType:r}}function S(e,n,i,r){var o,s=t.isArray(n),a=t.isPlainObject(n);t.each(n,function(n,u){o=t.type(u),r&&(n=i?r:r+"["+(a||"object"==o||"array"==o?n:"")+"]"),!r&&s?e.add(u.name,u.value):"array"==o||!i&&"object"==o?S(e,u,i,n):e.add(n,u)})}var i,r,e=0,n=window.document,o=/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,s=/^(?:text|application)\/javascript/i,a=/^(?:text|application)\/xml/i,u="application/json",f="text/html",c=/^\s*$/;t.active=0,t.ajaxJSONP=function(i,r){if(!("type"in i))return t.ajax(i);var f,h,o=i.jsonpCallback,s=(t.isFunction(o)?o():o)||"jsonp"+ ++e,a=n.createElement("script"),u=window[s],c=function(e){t(a).triggerHandler("error",e||"abort")},l={abort:c};return r&&r.promise(l),t(a).on("load error",function(e,n){clearTimeout(h),t(a).off().remove(),"error"!=e.type&&f?g(f[0],l,i,r):v(null,n||"error",l,i,r),window[s]=u,f&&t.isFunction(u)&&u(f[0]),u=f=void 0}),m(l,i)===!1?(c("abort"),l):(window[s]=function(){f=arguments},a.src=i.url.replace(/\?(.+)=\?/,"?$1="+s),n.head.appendChild(a),i.timeout>0&&(h=setTimeout(function(){c("timeout")},i.timeout)),l)},t.ajaxSettings={type:"GET",beforeSend:x,success:x,error:x,complete:x,context:null,global:!0,xhr:function(){return new window.XMLHttpRequest},accepts:{script:"text/javascript, application/javascript, application/x-javascript",json:u,xml:"application/xml, text/xml",html:f,text:"text/plain"},crossDomain:!1,timeout:0,processData:!0,cache:!0},t.ajax=function(e){var n=t.extend({},e||{}),o=t.Deferred&&t.Deferred();for(i in t.ajaxSettings)void 0===n[i]&&(n[i]=t.ajaxSettings[i]);p(n),n.crossDomain||(n.crossDomain=/^([\w-]+:)?\/\/([^\/]+)/.test(n.url)&&RegExp.$2!=window.location.host),n.url||(n.url=window.location.toString()),E(n),n.cache===!1&&(n.url=w(n.url,"_="+Date.now()));var s=n.dataType,a=/\?.+=\?/.test(n.url);if("jsonp"==s||a)return a||(n.url=w(n.url,n.jsonp?n.jsonp+"=?":n.jsonp===!1?"":"callback=?")),t.ajaxJSONP(n,o);var j,u=n.accepts[s],f={},l=function(t,e){f[t.toLowerCase()]=[t,e]},h=/^([\w-]+:)\/\//.test(n.url)?RegExp.$1:window.location.protocol,d=n.xhr(),y=d.setRequestHeader;if(o&&o.promise(d),n.crossDomain||l("X-Requested-With","XMLHttpRequest"),l("Accept",u||"*/*"),(u=n.mimeType||u)&&(u.indexOf(",")>-1&&(u=u.split(",",2)[0]),d.overrideMimeType&&d.overrideMimeType(u)),(n.contentType||n.contentType!==!1&&n.data&&"GET"!=n.type.toUpperCase())&&l("Content-Type",n.contentType||"application/x-www-form-urlencoded"),n.headers)for(r in n.headers)l(r,n.headers[r]);if(d.setRequestHeader=l,d.onreadystatechange=function(){if(4==d.readyState){d.onreadystatechange=x,clearTimeout(j);var e,i=!1;if(d.status>=200&&d.status<300||304==d.status||0==d.status&&"file:"==h){s=s||b(n.mimeType||d.getResponseHeader("content-type")),e=d.responseText;try{"script"==s?(1,eval)(e):"xml"==s?e=d.responseXML:"json"==s&&(e=c.test(e)?null:t.parseJSON(e))}catch(r){i=r}i?v(i,"parsererror",d,n,o):g(e,d,n,o)}else v(d.statusText||null,d.status?"error":"abort",d,n,o)}},m(d,n)===!1)return d.abort(),v(null,"abort",d,n,o),d;if(n.xhrFields)for(r in n.xhrFields)d[r]=n.xhrFields[r];var T="async"in n?n.async:!0;d.open(n.type,n.url,T,n.username,n.password);for(r in f)y.apply(d,f[r]);return n.timeout>0&&(j=setTimeout(function(){d.onreadystatechange=x,d.abort(),v(null,"timeout",d,n,o)},n.timeout)),d.send(n.data?n.data:null),d},t.get=function(){return t.ajax(j.apply(null,arguments))},t.post=function(){var e=j.apply(null,arguments);return e.type="POST",t.ajax(e)},t.getJSON=function(){var e=j.apply(null,arguments);return e.dataType="json",t.ajax(e)},t.fn.load=function(e,n,i){if(!this.length)return this;var a,r=this,s=e.split(/\s/),u=j(e,n,i),f=u.success;return s.length>1&&(u.url=s[0],a=s[1]),u.success=function(e){r.html(a?t("<div>").html(e.replace(o,"")).find(a):e),f&&f.apply(r,arguments)},t.ajax(u),this};var T=encodeURIComponent;t.param=function(t,e){var n=[];return n.add=function(t,e){this.push(T(t)+"="+T(e))},S(n,t,e),n.join("&").replace(/%20/g,"+")}}(Zepto),function(t){t.fn.serializeArray=function(){var n,e=[];return t([].slice.call(this.get(0).elements)).each(function(){n=t(this);var i=n.attr("type");"fieldset"!=this.nodeName.toLowerCase()&&!this.disabled&&"submit"!=i&&"reset"!=i&&"button"!=i&&("radio"!=i&&"checkbox"!=i||this.checked)&&e.push({name:n.attr("name"),value:n.val()})}),e},t.fn.serialize=function(){var t=[];return this.serializeArray().forEach(function(e){t.push(encodeURIComponent(e.name)+"="+encodeURIComponent(e.value))}),t.join("&")},t.fn.submit=function(e){if(e)this.bind("submit",e);else if(this.length){var n=t.Event("submit");this.eq(0).trigger(n),n.isDefaultPrevented()||this.get(0).submit()}return this}}(Zepto),function(t){"__proto__"in{}||t.extend(t.zepto,{Z:function(e,n){return e=e||[],t.extend(e,t.fn),e.selector=n||"",e.__Z=!0,e},isZ:function(e){return"array"===t.type(e)&&"__Z"in e}});try{getComputedStyle(void 0)}catch(e){var n=getComputedStyle;window.getComputedStyle=function(t){try{return n(t)}catch(e){return null}}}}(Zepto);
(function(mailru, $){
    $ = $ || {};
    /**
     * @global
     * @namespace
     * @alias mailru
     */
    var api = {},
        utils = {},
        callbacks = {},
        empty = function(){return false;},
        backButtonHandler = function(){return false;},
        defaultBackButtonHandler = function(){return false;},
        onPushCallback = function(){return false;},
        onPauseCallback = function(){return false;},
        onResumeCallback = function(){return false;},
        customBackButtonHandler = function(){return false;};

    function isUrl(url) {
        var parser = utils.parseURL(url);
        return parser.protocol;
    }

    function getValueByKey(o,k) {
        for (var tmp = k.split('.'),l = tmp.length, i = 0;i<l;i++) {
            if (!(o = o[tmp[i]])) break;
        }
        return o;
    }

    var validFuncs = {
        "string": function (param) {
            return typeof param === 'string';
        },
        "number": function(param) {
            return (typeof +param === 'number' && isNaN(+param) === false);
        },
        "url": function(param) {
            return isUrl(param);
        },
        "function": function(param) {
            return typeof param === 'function';
        }
    };

    function isValidParams(rules,params,errorCb) {
        function isPresent(param) {
            return param !== undefined && param !== null && param !== '' && !(typeof param == 'number' && isNaN(param));
        }

        if (arguments.length<3 && typeof params == 'function') {
            errorCb = params;
            params = undefined;
        }

        var rule, name, names, tmp, ruleList, param, i, l, n, isArray,
            maxLengthParam = '', maxLen = 0, valid = false, currentLen = 0, fullLen = 0;
        for (rule in rules) {
            if (!rules.hasOwnProperty(rule)) continue;
            names = rules[rule];
            ruleList = rule.split(',');
            if (!(names instanceof Array)) names = [names];
            for (n = 0; n < names.length; n++) {
                name = names[n];
                if (params) {
                    if (name.indexOf('.')>-1) {
                        param = getValueByKey(params,name);
                    } else {
                        param = params[name];
                    }
                } else {
                    param = name;
                    name = 0;
                }
                for (i = 0; i < ruleList.length; i++) {
                    rule = ruleList[i];

                    if (rule !== 'required' && !isPresent(param)) {
                        continue;
                    }

                    isArray = rule.indexOf('[]')>-1 && ((rule = rule.replace('[]','')),true);

                    switch(rule) {
                        case "required":
                            if (name.indexOf && ~name.indexOf('|')) {
                                param = name.split('|');
                                l = param.length;
                                while (l--) {
                                    if (valid = isPresent(params[param[l]])) {
                                        break;
                                    }
                                }
                            } else {
                                valid = isPresent(param);
                            }
                            break;
                        default:
                            if (isArray) {
                                if (valid = (param instanceof Array)) {
                                    for (var _i= 0,_l=param.length;_i<_l;_i++) {
                                        if (!(valid = validFuncs[rule](param[_i]))) break;
                                    }
                                }
                            } else {
                                valid = validFuncs[rule](param);
                                rule !== 'function' && (fullLen += (currentLen = encodeURIComponent(param).replace(/%../g,'x').length));
                            }
                    }
                    if (!valid) {
                        errorCb(rule,name,param);
                        return false;
                    }
                }
                if (maxLen < currentLen) {
                    maxLen = currentLen;
                    maxLengthParam = name;
                }
            }
            if (fullLen > 4e3) {
                errorCb('Any parameter is too long! May be it\'s "' + maxLengthParam + '"');
                return false;
            }
        }

        return valid;
    }

    function prepareParams(params,cb,map,def,errorcb) {
        if (typeof def == 'function') {
            errorcb = def;
            def = undefined;
        }
        if (typeof params == 'function') {
            params = {callback: params};
        }
        params = params || {};
        params.callback = params.callback || cb;

        if (def) for (var i in def) {
            params[i] = params[i] || def[i];
        }

        if (!isValidParams(map,params,errorcb || error)) {
            return false;
        }
        return params;
    }

    function prepareFunc(map,method,def,prepare) {
        if (typeof def == 'function') {
            prepare = def;
            def = undefined;
        }
        if (typeof method == 'function') {
            prepare = method;
            method = undefined;
        }
        return function(params,cb){
            if (params = prepareParams(params,cb,map,def)) {
                prepare && prepare(params);
                method && api.invoke(method, {
                    params: params,
                    cb: params.callback
                });
            } else {
                return false;
            }
        }
    }

    function addCallback(callback) {
        var cbid = utils.randomId();

        api.callbacks[cbid] = function(data) {
            delete api.callbacks[cbid];

            try {
                data = JSON.parse(data.replace(/\n/g,'\\n'));
            } catch(e) {

            }

            if (callback) {
                callback.call(window, data);
            }
        };

        return 'mailru.callbacks["' + cbid + '"]';
    }

    function error(rule,name,param) {
        var err;
        if (api.isDev) {
            if (arguments.length == 1) {
                err = typeof rule == 'string'? new Error(rule) : rule;
            } else {
                if (rule == 'required') {
                    err = new Error(isNaN(name)? 'Param '+name.replace(/\|/g,' or ')+' is required' : 'Missing required argument');
                } else {
                    err = new TypeError((isNaN(name)? 'Param '+name : 'Argument')+' must be a '+rule+', but is '+typeof(param));
                }
            }

            throw err;
        } else {
            return false;
        }
    }

    function saveCall(f,args,th) {
        try {
            return f.apply(th||this,args||[]);
        } catch (err) {
            error(err);
        }
    }

    utils.randomize = function() {
        return Math.floor(Math.random() * 10000);
    };
    utils.randomId = function() {
        return 'ID'+Math.random().toString(32).substring(2);
    };
    utils.parseURL = function(url) {
        var parser = document.createElement('a');
        parser.href = url;
        return parser;
    };
    utils.makeGet = function(obj) {
        if ('param' in $) {
            return $.param(obj);
        }

        var r = [];
        for (var k in obj) {
            if (!obj.hasOwnProperty(k)) continue;
            r[r.length] = k + '=' + encodeURIComponent(obj[k]);
        }
        return r.join('&');
    };

    if (mailru && mailru.invokeMessengerAPI) {
        window.oldMailru = mailru;

        api.invoke = function (methodName, data) {
            if (data && typeof data.cb == 'function')
                mailru.invokeMessengerAPI(methodName, JSON.stringify(data.params), addCallback(data.cb));
            else
                mailru.invokeMessengerAPI(methodName, JSON.stringify(data && data.params), 'mailru.emptycallback');
        };

        backButtonHandler = api.backButtonHandler = function() {
            var preventDefaultHandle = saveCall(customBackButtonHandler);
            if (!preventDefaultHandle) {
                mailru.backButtonHandler();
            }
            return preventDefaultHandle;
        };

        defaultBackButtonHandler = function() {
            mailru.backButtonHandler();
        }
    } else {
        api.invoke = function (methodName, data) {
            data = data || {};

            if (window.console) {
                window.console.log(methodName, data.params,addCallback(data.cb));
            }
            data.cb && data.cb({
                callbackTest: 'passed'
            });
        };
    }

    api.onPushData = function(d) {
        saveCall(onPushCallback,[d]);
    };

    api.onPause = api.onStop = function() {
        saveCall(onPauseCallback);
    };

    api.onResume = api.onStart = function() {
        saveCall(onResumeCallback);
    };

    api.utils = utils;
    api.callbacks = {};
    api.emptycallback = empty;

    /**
     * @namespace
     */
    api.app = {};
    /**
     * @namespace
     */
    api.device = {};
    /**
     * @namespace
     */
    api.message = {};
    /**
     * @namespace
     */
    api.users = {};
    /**
     * @namespace
     */
    api.friends = {};
    /**
     * @namespace
     */
    api.photos = {};

    /**
     * Закрывает приложение и возвращает пользователя в предыдущий контекст (каталог приложений/сообщение)
     * @function close
     * @memberof mailru.app
     * @param {boolean} [force] - если передано значение true происходит форсированное закрытие приложение,
     * можно использовать в случае, если обычное закрытие по какой-то причине не работает.
     */
    api.app.close = function(force) {
        if (force) {
            api.invoke('app.close');
        } else {
            defaultBackButtonHandler();
        }
    };
    /**
     * Объект для работы с экранной клавиатурой
     * @namespace keyboard
     * @name keyboard
     * @memberof mailru.app
     */
    api.app.keyboard = {};
    /**
     * Скрывает экранную клавиатуру
     * @function hide
     * @memberof mailru.app.keyboard
     */
    api.app.keyboard.hide = function() {
        api.invoke('keyboard.hide');
    };
    /**
     * Показывает экранную клавиатуру
     * @function show
     * @memberof mailru.app.keyboard
     */
    api.app.keyboard.show = function() {
        api.invoke('keyboard.show');
    };

    /**
     * Устанавливает или сбрасывает обработчик нажатия кнопки Back
     * @function back
     * @name back(1)
     * @memberof mailru.app
     * @param {function|boolean} handler -  функция, которая будет вызвана при нажатии кнопки Back
     * если handler возвращает true - стандартное поведение при клике на Back будет игнорироваться.
     * Если в качестве параметра handler передано значение boolean равное false, то текущий, установленный обработчик
     * кнопки Back будет удален.
     * @return {boolean} Если false значит метод выполнен с ошибками.
     * @example
     * var isFirstPage = true;
     * mailru.app.back(function(){
     *   if (!isFirstPage) {
     *     alert('Go to previous page');
     *     return true;
     *   } else {
     *     alert('Go away from application');
     *     return false;
     *   }
     * });
     */
    /**
     * При вызове без параметров имитирует нажатие на кнопку Back
     * @function
     * @name back(2)
     * @memberof mailru.app
     * @return {boolean} =false Возвращает значение, которое вернул обработчик нажатия кнопки Back
     */
    api.app.back = api.customBackButtonHandler = function(handler) {
        if (handler === false) handler = function(){return false};
        if (handler) {
            if (isValidParams({
                    'required,function': handler
                }, function(){
                    error.apply(null,arguments);
                })) {
                customBackButtonHandler = handler;
                return true;
            } else {
                return false;
            }
        } else {
            return backButtonHandler();
        }
    };
    /**
     * Устанавливает обработчик данных уведомления, который вызывается при получении нового уведомления
     * при активном экране с приложением
     * @function onPush
     * @memberof mailru.app
     * @param {function} cb - обработчик данных, пришедших в пуше
     * @param {object} cb.data - данные, которые были переданы в параметре data при отправке уведомления
     * @return {boolean} Если false значит метод выполнен с ошибками.
     */
    api.app.onPush = api.app.setOnPushDataCallback = function(cb) {
        if (cb === false) cb = function(){};
        if (isValidParams({
                'required,function': cb
            }, function(){
                error.apply(null,arguments);
            })) {
            onPushCallback = cb;
            return true;
        } else {
            return false;
        }
    };
    /**
     * Устанавливает обработчик состояния активности приложения.
     * Приложение является активным, когда показывается пользователю.
     * Если открывается диалог через api, пользователь сворачивает клиент, etc - приложение уходит в бэкграунд и вызывается onPause.
     * После возвращения пользователя в приложение - вызывается [onResume]{@link mailru.app.onResume}
     * @function onPause
     * @memberof mailru.app
     * @param {function} cb - обработчик состояния активности приложения.
     * @return {boolean} Если false значит метод выполнен с ошибками.
     */
    api.app.onPause = function(cb) {
        if (cb === false) cb = function(){};
        if (isValidParams({
                'required,function': cb
            }, function(){
                error.apply(null,arguments);
            })) {
            onPauseCallback = cb;
            return true;
        } else {
            return false;
        }
    };
    /**
     * Устанавливает обработчик состояния активности приложения.
     * Приложение является активным, когда показывается пользователю.
     * Если открывается диалог через api, пользователь сворачивает клиент, etc - приложение уходит в бэкграунд и вызывается [onPause]{@link mailru.app.onPause}.
     * После возвращения пользователя в приложение - вызывается onResume
     * @function onResume
     * @memberof mailru.app
     * @param {function} cb - обработчик состояния активности приложения.
     * @return {boolean} Если false значит метод выполнен с ошибками.
     */
    api.app.onResume = function(cb) {
        if (cb === false) cb = function(){};
        if (isValidParams({
                'required,function': cb
            }, function(){
                error.apply(null,arguments);
            })) {
            onResumeCallback = cb;
            return true;
        } else {
            return false;
        }
    };
    api.app.loaded = function() {
        try {
            api.invoke('app.loaded');
        } catch (e) {}
    };

    api.app.openUrl = prepareFunc({
        'required,url': 'url'
    }, 'app.openUrl');

    /**
     * @callback mailru.device~callback
     * @param {{}} result - Состояние ориентации устройства
     * @param {string} result.orientation - текущая ориентация, может иметь значения landscape, или portrait.
     * @param {boolean} result.lock - блокировка смены ориентации - false -  меняется в зависимости от положения устройства, true - не меняется.
     */
    /**
     * Возвращает ориентацию и блокировку смены ориентации
     * @function orientation
     * @name orientation(1)
     * @memberof mailru.device
     * @param {mailru.device~callback} callback - принемает состояние ориентации устройства.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    /**
     * Устанавливает ориентацию и блокировку смены ориентации
     * @function
     * @name orientation(2)
     * @variation 2
     * @memberof mailru.device
     * @param {{}} params
     * @param {string} [params.orientation] - новая ориентация, может иметь значения landscape, или portrait.
     * @param {boolean} [params.lock] - блокировка смены ориентации - false -  меняется в зависимости от положения устройства, true - не меняется.
     * @param {mailru.device~callback} [params.callback] - принемает состояние ориентации устройства.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.device.orientation = prepareFunc({
        'string': 'orientation',
        'function': 'callback'
    },'device.orientation');

    /**
     * Выполняет запрос на отправку сообщения пользователю из контакт-листа юзера.
     * Обязательным параметром является один из params.text, params.url, params.title, params.image.
     * @function send
     * @memberof mailru.message
     * @param {{}} params
     * @param {string} params.text - текст сообщения
     * @param {url} params.url|image - ссылка на картинку
     * @param {string} params.title - заголовок сообщения
     * @param {string} [params.uin] - uin получателя, если передан - будет предвыбран на экране выбора получателей
     * @param {object} [params.data] - данные, которые приложение получит при открытии
     * @param {{text: string}} [params.fallback]
     * @param {string} [params.fallback.text] - текст, который увидит пользователь клиента, не поддерживающего сообщения из приложений (старые клиенты)
     * @param {bool} [params.only_compatible] - фильтрация списка получателей, если не передан uin. only_compatible=0 - показываются все получатели, only_compatible=1 - показываются только получатели, которые поддерживают приложения хотя бы на одном инстансе. необязательный параметр
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.message.send = prepareFunc({
        'required': 'text|url|title|image|base64',
        'string': ['text','title','uin','base64','fallback.text'],
        'url': ['url','image'],
        'function': 'callback'
    },'message.send',function(params){
        if (typeof params.data === 'object') {
            params.data = utils.makeGet(params.data);
        }
    });
    /**
     * Открытие диалога с пользователем
     * @function openChat
     * @memberof mailru.message
     * @param {{uin: string}} params
     * @param {string} params.uin
     * @param {function} params.callback
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.message.openChat = prepareFunc({
        'string': 'uin',
        'function': 'callback'
    },'message.openChat');

    /**
     * @callback mailru.users~getInfo_callback
     * @param {object} result - Информация о пользователе
     * @param {string} result.first_name - имя
     * @param {string} result.last_name - фамилия
     * @param {string} result.nick - ник
     * @param {number} result.sex - пол --- 0 - мужской, 1 - женский, 2 - не задан
     * @param {date} result.birthday - дата рождения в формате dd.mm.yyyy
     * @param {object} result.avatars
     * @param {url} result.avatars.big - ссылка на аватарку
     * @param {url} result.avatars.small - ссылка на аватарку
     * @param {number} result.online - статус пользователя - 0 - оффлайн, 1 - в сети
     * @param {string} result.uin - uin
     * @param {number} result.contacts_count - кол-во контактов в кл,
     * @param {number} result.apps_support - флаг поддержки клиентом приложений
     */
    /**
     * Возвращает инфо по текущему пользователю
     * @function getInfo
     * @name getInfo(1)
     * @memberof mailru.users
     * @param {mailru.users~getInfo_callback} callback - принемает информацию о пользователе.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    /**
     * Возвращает инфо по указанному uin.
     * @function
     * @name getInfo(2)
     * @variation 2
     * @memberof mailru.users
     * @param {{}} params
     * @param {string} [params.uin] - uin пользователя, по которому нужно получить информацию.
     * @param {mailru.users~getInfo_callback} params.callback - принемает информацию о пользователе.
     * @example
     * mailru.users.getInfo(function(info){alert('Hello '+info.first_name)});
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.users.getInfo = prepareFunc({
        'required,function': 'callback',
        'string': 'uin'
    },'users.getInfo');

    /**
     * @callback mailru.users~getLocation_callback
     * @param {{}} result - Текущие гео-данные пользователя
     * @param {{lat: float, lon: float}} result.location - текущие координаты формата {lat: float, lon: float}
     * @param {string} result.status - статус выполнения [success/fail]
     */
    /**
     * Возвращает текущие гео-данные пользователя.
     * @function getLocation
     * @memberof mailru.users
     * @param {mailru.users~getLocation_callback} callback - принемает информацию о пользователе.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     * @example
     * mailru.users.getLocation(function(data){
     *   if (data.status == 'success') {
     *     alert('Your location is lat: '+data.location.lat+', lon: '+data.location.lon)});
     *   } else {
     *     alert('Sorry, '+data.message);
     *   }
     * });
     */
    api.users.getLocation = prepareFunc({
        'required,function': 'callback'
    },'users.getLocation');

    /**
     * @callback mailru.friends~add_callback
     * @param {{}} result
     * @param {string} result.status - статус выполнения [success/already/error/fail],
     * success - успешное добавление, already - uin уже есть в контакт-листе,
     * fail - пользователь отказался добавлять переданый uin,
     * error - ошибка в процессе выполнения вызова
     */
    /**
     * Запрос на добавление в контакт-лист.
     * показывает диалог добавления пользователя в контакт лист.
     * @function add
     * @memberof mailru.friends
     * @param {{}} params
     * @param {string} params.uin - uin пользователя, которого нужно добавить в друзья активному пользователю.
     * @param {mailru.friends~add_callback} [params.callback] - функция для обработки результата.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     * @example
     * mailru.friends.add({
	 *   uin: 111111111,
	 *   callback: function(res){
	 *     var messages = {
     *       success: 'User is successful added',
     *       already: 'User already in your contactlist',
     *       fail: 'User reject your request',
     *       error: 'Sorry, we have any error'
     *     };
     *     alert(messages[res.status] || messages.error);
     *   }
     *});
     */
    api.friends.add = prepareFunc({
        'required,string': 'uin',
        'function': 'callback'
    },'friends.add');

    /**
     * @callback mailru.friends~pick_callback
     * @param {object} result
     * @param {(Object[])} result.users - информация о выбранных пользователях
     * @param {string} result.users.first_name - имя
     * @param {string} result.users.last_name - фамилия
     * @param {string} result.users.nick - ник
     * @param {number} result.users.sex - пол --- 0 - мужской, 1 - женский, 2 - не задан
     * @param {date} result.users.birthday - дата рождения в формате dd.mm.yyyy
     * @param {object} result.users.avatars
     * @param {url} result.users.avatars.big - ссылка на аватарку
     * @param {url} result.users.avatars.small - ссылка на аватарку
     * @param {number} result.users.online - статус пользователя - 0 - оффлайн, 1 - в сети
     * @param {string} result.users.uin - uin
     * @param {number} result.users.contacts_count - кол-во контактов в кл,
     * @param {number} result.users.apps_support - флаг поддержки клиентом приложений
     * @param {string} result.status - статус выполнения [success/error/fail]
     */
    /**
     * Показывает диалог выбора пользователей из контакт-листа. после выбора и закрытия диалога приложение получает список выбранных пользователей
     * @function pick
     * @name pick(1)
     * @memberof mailru.friends
     * @param {mailru.friends~pick_callback} callback - принемает информацию о пользователях.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    /**
     * Позволяет задать параметры фильтрации для выбора пользователей из контакт-листа.
     * @function
     * @name pick(2)
     * @memberof mailru.friends
     * @variation 2
     * @param {{}} params
     * @param {number} [params.limit] - ограничение кол-ва выбираемых людей. необязательный параметр. по умолчанию - нет ограничения
     * @param {boolean} [params.only_compatible] - фильтрация списка получателей, если не передан uin. only_compatible=0 - показываются все получатели, only_compatible=1 - показываются только получатели, которые поддерживают приложения хотя бы на одном инстансе. необязательный параметр
     * @param {string[]} [params.preselect] - массив uin'ов, которые будут заранее помечены выбраннными.
     * @param {mailru.friends~pick_callback} params.callback - принемает информацию о выбранных пользователях.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.friends.pick = prepareFunc({
        'number': 'limit',
        'string[]': 'preselect',
        'required,function': 'callback'
    },'friends.picker',{limit:0});

    /**
     * Запрос на получение данных о пользователях из контакт-листа юзера.
     * Метод возвращает uin'ы и минимальную инфу о пользователях из кл текущего пользователя.
     * @function getInfo
     * @name getInfo(1)
     * @memberof mailru.friends
     * @param {mailru.friends~pick_callback} callback - принемает информацию о пользователях.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     * @example
     * mailru.friends.getInfo(function(data){
     *   if (data.status == 'success') {
     *     alert('You have '+data.users.length+' friends');
     *   } else {
     *     alert('Sorry, we have any error');
     *   }
     * });
     */
    /**
     * Метод возвращает uin'ы и минимальную инфу о пользователях из кл текущего пользователя.
     * Позволяет задать параметры фильтрации
     * @function
     * @name getInfo(2)
     * @variation 2
     * @memberof mailru.friends
     * @param {{}} params
     * @param {number} [params.offset=0] - задает смещение в выборке, нужно если у пользователя контактов больше, чем лимит
     * @param {number} [params.limit=500] - задает лимит кол-ва получаемых контактов
     * @param {boolean} [params.extended=false] - не обязательный параметр. если передан true - возвращает полный набор полей о пользователях, но будет происходить долгий сетевой запрос, если false - будет возвращен урезанный набор полей - nick, online, uin, avatars
     * @param {mailru.friends~pick_callback} params.callback - принемает информацию о пользователях.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     * @example
     * mailru.friends.getInfo({
     *   offset: 1,
     *   limit: 1,
     *   callback: function(data){
     *     if (data.status == 'success') {
     *       alert('Your second friend is '+data.users[0].nick);
     *     } else {
     *       alert('Sorry, we have any error');
     *     }
     *   }
     * });
     */
    api.friends.getInfo = prepareFunc({
        'number': ['limit','offset'],
        'required,function': 'callback'
    },function(params){
        api.invoke('friends.getInfo'+(params.extended? 'Extended' : ''), {
            params: params,
            cb: function(data){
                try{
                    data = JSON.parse(data.replace(/\n/g,'\\n'));
                } catch(e){}
                params.callback(data);
            }
        });
    },{limit: 100, offset: 0});

    /**
     * @callback mailru.photos~callback
     * @param {{}} result
     * @param {string_base64[]} result.photos - массив выбранных изображений в формате base64
     * @param {string} result.status - статус выполнения [success/error/fail]
     */
    /**
     * Показывает диалог выбора фото из галереи телефона/снять новое фото. после выбора фото и закрытия диалога приложение получает фото в формате base64. если возможен - нативный мультивыбор
     * @function get
     * @memberof mailru.photos
     * @param {mailru.photos~callback} callback - функция для обработки результата.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.photos.get = prepareFunc({
        'required,function': 'callback'
    },'photos.get');

    api.photos.save = prepareFunc({
        'required': 'url|base64',
        'string': 'base64',
        'url': 'url'
    },'photos.save');

    api.photos.upload = prepareFunc({
        'required': 'url|base64',
        'string': 'base64',
        'url': 'url'
    },'photos.upload');

    window.mailru = api;
})(window.mailru, window.$);

(function(w,d){
    function App() {
        var touchSupported = 'ontouchstart' in document;
        var pages = {}, loads = {}, unloads = {}, history = [], current = [], handlers = {};
        var pagesInited, handlersAdded;
        window.__pages = pages;
        window.__loads = loads;
        window.__unloads = unloads;
        window.__history = history;
        var transitionFunc;
        var activeClass = 'active';
        var transition = 'default';
        var activeDelay = 30;

        var buttActions = {

        };

        var buttClassActions = {
            'app-tab-button': function(b,act,t){
                var h, c, tabs = (getParent(b,'.app-tab-bar') || b.parentNode).querySelectorAll('.app-tab-button');
                if (tabs.length>1 && !hasClass(b,'tab-active')) {
                    var activeDetected, leftActive, shw = [], hid = [], trFn = (b.getAttribute('data-options')||'').split(':');
                    addClass(b,'tab-active');
                    if ((h = b.getAttribute('href')) && (c = document.querySelectorAll(h)).length) {
                        shw.push.apply(shw,arSlice(c,0));
                    }
                    arrayForEach.call(tabs,function(it){
                        if (it !== b) {
                            activeDetected || leftActive || (leftActive = hasClass(it,'tab-active'));
                            removeClass(it,'tab-active');
                            if ((h = it.getAttribute('href')) && (c = document.querySelectorAll(h)).length) {
                                hid.push.apply(hid,arSlice(c,0));
                            }
                        } else {
                            activeDetected = true;
                        }
                    });
                    (_transitions[trFn[0]] || _transitions['default']).apply(null,[hid,shw,leftActive].concat((trFn[1]||[]).slice(1)));
                }

            }
        };

        var tmp = ['ease','ease-in','ease-out'], easeTypes = {};
        var _transitions = {
            'default': function(h,s){
                var l = h.length;
                while (l--) h[l].style.display = 'none';
                l = s.length;
                while (l--) s[l].style.display = 'block';
            },
            'swipe': function(h,s,dirLR,rev,delay,type){
                if (!isNaN(rev) && !delay) {
                    type = delay
                    delay = rev;
                    rev = false;
                }
                (delay *= 1) || (delay = 300);
                rev = rev == 'rev';
                var w,l = h.length;
                dirLR = dirLR? 1 : -1;
                rev && (dirLR *= -1);
                while (l--) {
                    w = dirLR * h[l].offsetWidth;
                    if (h[l].__in_transition) clearTimeout(h[l].__in_transition);
                    h[l].style.webkitTransition = h[l].style.transition = delay+'ms '+(easeTypes[type]||'');
                    (function(el){
                        setTimeout(function(){
                            el.style.webkitTransform = el.style.transform = 'translateX('+(-w)+'px)';
                            el.__in_transition = setTimeout(function(){
                                el.style.display = 'none';
                                el.style.webkitTransform = el.style.transform =
                                    el.style.webkitTransition = el.style.transition = '';
                                delete el.__in_transition;
                            },delay+20);
                        },1);
                    })(h[l]);
                }
                l = s.length;
                while (l--) {
                    if (!s[l].offsetWidth) {
                        s[l].style.visibility = 'hidden';
                        s[l].style.display = 'block';
                    }
                    w = dirLR * s[l].offsetWidth;
                    if (s[l].__in_transition) {
                        clearTimeout(s[l].__in_transition);
                    } else {
                        s[l].style.webkitTransform = h[l].style.transform = 'translateX('+w+'px)';
                    }
                    s[l].style.visibility = '';
                    (function(el){
                        setTimeout(function(){
                            el.style.webkitTransition = el.style.transition = delay+'ms '+(easeTypes[type]||'');
                            el.style.webkitTransform = el.style.transform = 'translateX(0px)';
                            el.__in_transition = setTimeout(function(){
                                el.style.webkitTransform = el.style.transform =
                                    el.style.webkitTransition = el.style.transition = '';
                                delete el.__in_transition;
                            },delay+20);
                        },1);
                    })(s[l]);
                }
            }
        };

        var transitions = {
            'default': function(h,s){
                h && hidePage(h);
                s && showPage(s);
            }
        };

        function arSlice (a,n) {
            return Array.prototype.slice.call(a,n);
        }

        var indexOfArray = (function(){
            if (typeof Array.prototype.indexOf == 'function') return Array.prototype.indexOf;
            else return function(it){
                var l = this.length;
                while (l--) {
                    if (it === this[l]) return l;
                }
                return -1;
            }
        })();

        var arrayForEach = (function(){
            if (typeof Array.prototype.forEach == 'function') return Array.prototype.forEach;
            else return function(f){
                for (var i=0,l=this.length;i<l;i++) {
                    f(this[i],i);
                }
            }
        })();

        arrayForEach.call(tmp,function(it){easeTypes[it] = it});

        function runFuncsArray(ar) {
            var args = arSlice(arguments,1),th = this;
            arrayForEach.call(ar,function(f){f.apply(th,args)});
        }

        function fireMouseEvent (n, t, x, y) {
            var e = document.createEvent('MouseEvents');
            e.initMouseEvent(t, true, true, window, 1, x, y, x, y, false, false, false, false, 0, null);
            n.dispatchEvent(e);
        }

        function _f(f){
            return function(){
                try {
                    return f.apply(this,arguments);
                } catch(err){
                    alert(err);
                    //console.log(err.toString());
                }
            }
        }

        function extend(t,c,force) {
            for (var i in c) {
                if (c.hasOwnProperty(i) && (force || !t.hasOwnProperty(i))) {
                    t[i] = c[i];
                }
            }
            return t;
        }

        var appHandlers = {}, triggered = {};
        function AppEvent(t,target,p,d) {
            extend(this,p);
            var data = extend({},p);
            this.type = t;
            this.target = target;
            this.stoped = false;
            this.prevented = false;
            this.canStop || (this.canStop = false);
            this._getData = function(){ return data };
        }
        AppEvent.prototype.stop = function(){
            this.stoped = true;
        };
        function bindHandler(t,h,c,args) {
            if (typeof t == 'function') {
                args = c;
                c = h;
                h = t;
                t = 'anyevent';
            }
            (args instanceof Array) || (args = null);
            var ho = [h,c||null,args||[],false];
            (appHandlers[t] || (appHandlers[t] = [])).push(ho);
            return {
                type: t,
                handler: h,
                context: c,
                params: args,
                isUnbinded: function(){ return ho[3]; },
                call: function(e){ return h.apply(c||null,[e].concat(args||[])); },
                unbind: function(){ return ho[3] || unbindHandler(t,h,c,args); },
                bind: function(){ return ho[3] && extend(this,bindHandler(this.type,this.handler,this.context,this.params),true); }
            }
        }
        function unbindHandler(t,h,c,args) {
            if (!t) {
                var v = true;
                for (var i in appHandlers) {
                    unbindHandler(i) || (v = false);
                }
                return v;
            }
            var hs = appHandlers[t], l = (hs || []).length;
            while (l--) {
                if ((!h || hs[l][0]===h) && (!c || hs[l][1]===c) && (!args || hs[l][2]===args)) {
                    hs[l][3] = true;
                    hs.splice(l,1);
                    if (h) return true;
                }
            }
            if (!h && hs) return true;
            return false;
        }
        function triggerEvent(target,t,p) {
            var value = true, trig, isAny = t === 'anyevent';
            if (triggered[t]) return false;
            isAny || (triggered[t] = true);
            if (appHandlers[t] && appHandlers[t].length) {
                trig = true;
                p = p instanceof AppEvent? p : new AppEvent(t,target,p);
                t = appHandlers[t].slice(0);
                for (var i= 0,l=t.length;i<l;i++) {
                    (t[i][0].apply(t[i][1],[p].concat(t[i][2])) === false) && (p.prevented = !(value = false));
                    if (p.stoped) return value;
                }
            }
            if (!isAny) {
                triggerEvent(target,'anyevent',trig? p : new AppEvent(t,target,p))===false && (value = false);
                delete triggered[trig? p.type : t];
            }
            return value;
        }

        function initPages() {
            if (pagesInited) return;
            pagesInited = true;
            var ps = d.getElementsByClassName('app-page');
            var pn,l = ps.length;
            while (l--) {
                if (!pages[pn = ps[l].getAttribute('data-page')]) {
                    pages[pn] = ps[l].cloneNode(true);
                    ps[l].parentNode.removeChild(ps[l]);
                }
            }
            if (d.body) {
                addClass(d.body,'app-loaded');
                bindHandler(function(e){
                    var ev, dat = e._getData();
                    if ('clientX' in dat) {
                        ev = d.createEvent('MouseEvent');
                        ev.initMouseEvent(e.type, true, true, window, 1, e.clientX||0, e.clientY||0, e.clientX||0, e.clientY||0,
                            false, false, false, false, 0, null);
                        extend(ev,dat,true);
                    } else {
                        ev = d.createEvent('Event');
                        ev.initEvent(e.type, true,true);
                    }
                    ev._args = dat;
                    return (e.target instanceof Element? e.target : d.body).dispatchEvent(ev);
                });
            }
        }
        function hidePage(pg) {
            pg[1].style.display = 'none';
            setTimeout(function(){
                pg[1].parentNode && pg[1].parentNode.removeChild(pg[1]);
                triggerEvent(pg[1], 'page:hide', {pageName: pg[0], pageContext: pg[2], pageData: pg[3]});
            },1);
        }
        function showPage(pg) {
            pg[1].style.display = 'block';
            d.body && pg[1].parentNode || d.body.appendChild(pg[1]);
            triggerEvent(pg[1], 'page:show', {pageName: pg[0], pageContext: pg[2], pageData: pg[3]});
        }
        function switchPages(hide,shows,withoutTrigger) {
            shows[1].style.display = 'none';
            d.body && shows[1].parentNode || d.body.appendChild(shows[1]);
            withoutTrigger || triggerEvent(shows[1], 'page:change', {oldPage: hide && buildPageObject(hide).name, newPage: buildPageObject(shows).name});
            transitionFunc(hide,shows);
        }
        function addClass(n,c) {
            if (c && !hasClass(n,c)) {
                n.className += (' '+c);
            }
        }
        function removeClass(n,c) {
            c && (n.className = n.className.replace(new RegExp('(\\s|^)('+ c.replace(/\s/g,'|')+')(\\s|$)','g'),' ').replace(/(^\s+|\s+$)/g,''));
        }
        function hasClass(n,c) {
            return n.className.search(new RegExp('(\\s|^)'+c+'(\\s|$)'))>-1
        }
        function _customSelectorFunc(s) {
            var tested = false,v = true, n = this;
            s.replace(/(^[a-zA-Z0-9]+)|(\.\w+)|\[(\w+)(?:=(\w+))?\]/g,function(m,tn,cls,an,av){
                if (!v) return '';
                tested = true;
                switch (m.charAt(0)) {
                    case '.': v = hasClass(n,cls.substring(1));break;
                    case '[': v = n.getAttribute(an);
                        if (typeof av != 'undefined') {
                            v = v==av;
                        }
                        break;
                    default: v = n.tagName.toLowerCase() == (tn||'').toLowerCase();
                }
            });
            return tested && !!v;
        }
        function getParent(n,s) {
            var testFn = n.matches || n.webkitMatchesSelector || _customSelectorFunc;
            while (n) {
                if (testFn.call(n,s)) {
                    return n;
                }
                if ((n = n.parentNode)===document) return false;
            }
            return false;
        }
        if (touchSupported && Event.prototype.stopImmediatePropagation) {
            var firstClickHandler = function(e){
                if (e.target.type == 'submit' && !e.x && !e.y) return;
                //console.log(e.target.__stopClick__===this,e.target.__canClick__,e.target.__stopClick__,this, e.target,e);
                if (e.target.__stopClick__===this || !e.target.__canClick__) {
                    delete e.target.__stopClick__;
                    delete e.target.__canClick__;
                    if (e.stopImmediatePropagation) {
                        e.stopImmediatePropagation();
                    }
                    e.preventDefault();
                    e.stopPropagation();
                    e.cancelBubble = true;
                    e.returnValue  = false;
                    return false;
                }
                e.target.__stopClick__ || (e.target.__stopClick__ = this);
            };
            var proto = typeof EventTarget == 'function'? EventTarget : Node;
            if (proto) {
                proto = proto.prototype;
                if (!proto._addEventListener) {
                    proto._addEventListener = proto.addEventListener;
                    proto.addEventListener = function(t,h){
                        if (t==='click' && typeof h == 'function' && !~indexOfArray.call(this.__ch__ || (this.__ch__ = []),h)) {
                            if (!this.__ch__.length) {
                                this._addEventListener('click',firstClickHandler);
                            }
                            this.__ch__.push(h);
                        }
                        return this._addEventListener.apply(this,arguments);
                    }
                }
                if (!proto._removeEventListener) {
                    proto._removeEventListener = proto.removeEventListener;
                    proto.removeEventListener = function(t,h){
                        if (t==='click' && typeof h == 'function') {
                            var ind = indexOfArray.call(this.__ch__ || (this.__ch__ = []),h);
                            if (~ind) {
                                this.__ch__.splice(ind,1);
                                this.__ch__.length || this._removeEventListener('click',firstClickHandler);
                            }
                        }
                        return this._removeEventListener.apply(this,arguments);
                    }
                }
                if (!proto._dispatchEvent) {
                    proto._dispatchEvent = proto.dispatchEvent;
                    proto.dispatchEvent = function(e){
                        if (e && e.type == 'click') {
                            this.__canClick__ = true;
                            delete this.__stopClick__;
                        }
                        return this._dispatchEvent.apply(this,arguments);
                    }
                }
                if (Element && (proto = Element.prototype)) {
                    if (!proto.__emulateClick) {
                        proto.__emulateClick = proto.click;
                        proto.click = function(){
                            this.__canClick__ = true;
                            delete this.__stopClick__;
                            return this.__emulateClick.apply(this,arguments);
                        }
                    }
                }
                if (HTMLElement && (proto = HTMLElement.prototype)) {
                    if (!proto.__emulateClick) {
                        proto.__emulateClick = proto.click;
                        proto.click = function(){
                            this.__canClick__ = true;
                            delete this.__stopClick__;
                            return this.__emulateClick.apply(this,arguments);
                        }
                    }
                }
            }
        }

        var Swapper = (function(){
            var div, startBuffer = 5;
            function start(sw,touch) {
                div = {
                    dx: touch.clientX - sw.start.clientX,
                    dy: touch.clientY - sw.start.clientY,
                    clientX: touch.clientX,
                    clientY: touch.clientY
                };
                if (Math.abs(div.dx)>startBuffer || Math.abs(div.dy)>startBuffer) {
                    sw.current = sw.start = touch;
                    triggerEvent(sw.target,'swap:start',div);
                    onMove = move;
                }
            }
            function move(sw,touch) {
                div = {
                    dx: touch.clientX - sw.current.clientX,
                    dy: touch.clientY - sw.current.clientY,
                    deltaX: touch.clientX - sw.start.clientX,
                    deltaY: touch.clientY - sw.start.clientY,
                    clientX: touch.clientX,
                    clientY: touch.clientY
                };
                sw.current = touch;
                triggerEvent(sw.target,'swap:move',div);
            }
            var onMove = start;
            return {
                onMove: function(sw,e){
                    onMove(sw, e.changedTouches[0]);
                },
                onEnd: function(sw,e){
                    onMove = start;
                    div = sw.current? {
                        deltaX: sw.current.clientX - sw.start.clientX,
                        deltaY: sw.current.clientY - sw.start.clientY,
                        clientX: sw.current.clientX,
                        clientY: sw.current.clientY
                    } : {deltaX:0, deltaY:0, clientX: sw.start.clientX, clientY: sw.start.clientY };
                    triggerEvent(sw.target,'swap:end',div);
                }
            }
        })();

        var makeButtons = touchSupported? function (n,hs) {
            var started,butt,actClass,canClick,activeTimeout;
            var swap;
            function clear(){
                started = false;
                if (butt) {
                    activeTimeout && (activeTimeout = clearTimeout(activeTimeout) && false);
                    butt && removeClass(butt,actClass);
                    butt = actClass = undefined;
                }
            }
            addHandler(hs,n,'touchstart',function(e){
                if (butt) clear();
                if (swap) swap = Swapper.onEnd(swap,e);
                if (e.changedTouches.length>1) return;
                butt = getParent(e.target,'.app-button');
                swap = {start: e.changedTouches[0], target: started = e.target};
                if (butt) {
                    actClass = butt.getAttribute('data-clickable-class') || activeClass;
                    activeTimeout = setTimeout(function(){
                        activeTimeout = false;
                        addClass(butt,actClass);
                    },activeDelay);
                }
            });
            addHandler(hs,n,'touchmove', function(e){
                if (started) {
                    clear();
                }
                Swapper.onMove(swap,e);
            });
            addHandler(hs,n,'touchcancel',function(e){
                clear();
                if (swap) swap = Swapper.onEnd(swap,e);
            });
            addHandler(hs,n,'touchend',_f(function(e){
                if (swap) swap = Swapper.onEnd(swap,e);
                if (!started) return;
                e.target.__canClick__ = started=== e.target;
                delete e.target.__stopClick__;
                started = false;
                if (butt && butt === getParent(e.target,'.app-button')) {
                    if (activeTimeout) {
                        clearTimeout(activeTimeout);
                        activeTimeout = false;
                        addClass(butt,actClass || activeClass);
                    }
                    activeTimeout = setTimeout(function(){
                        activeTimeout = false;
                        clear();
                    },50);
                    if ( e.stopImmediatePropagation) {
                        fireMouseEvent(e.target,'click',e.changedTouches[0].clientX, e.changedTouches[0].clientY);
                    }
                }
            }));
            addHandler(hs,n,'click',_f(function(e){
                clear();
            }));

        } : function (n,hs) {
            var started,butt,actClass,activeTimeout,tm;
            function clear(){
                started = false;
                activeTimeout && (activeTimeout = clearTimeout(activeTimeout) && false);
                butt && removeClass(butt,actClass);
            }
            addHandler(hs,n,'mousedown',function(e){
                if (tm || started) {
                    clearTimeout(tm);
                    tm = false;
                    clear();
                }
                butt = getParent(e.target,'.app-button');
                if (butt) {
                    started = true;
                    actClass = butt.getAttribute('data-clickable-class') || activeClass;
                    activeTimeout = setTimeout(function(){
                        activeTimeout = false;
                        addClass(butt,actClass);
                    },activeDelay);
                }
            });
            addHandler(hs,n,'mousemove',function(e){
                if (!started) return;
                clear();
            });
            addHandler(hs,n,'mouseup',_f(function(e){
                if (!started) return;
                if (butt && butt === getParent(e.target,'.app-button') && activeTimeout) {
                    clearTimeout(activeTimeout);
                    activeTimeout = false;
                    addClass(butt,actClass);
                    tm = setTimeout(function(){tm = false; clear();},5);
                } else clear();
            }));
        };

        /*function addHandler(hs,n,t,h) {
         var H = h,
         types = t.split(' '),
         l = types.length;//,
         args = arSlice(arguments,4);
         if (args.length) {
         H = function(e){
         return h.apply(this,args);
         };
         }
         while (l--) {
         if (types[l].indexOf(':atfirst')>-1) {
         types[l] = types[l].replace(':atfirst','');
         hs.push([n,types[l],H]);
         n.addClickListenerAtFirst(H);
         } else {
         hs.push([n,types[l],H]);
         n.addEventListener(types[l],H,false);
         }
         }
         }*/
        function addHandler(hs,n,t,h) {
            var H = h,
                types = t.split(' '),
                l = types.length;
            args = arSlice(arguments,4);
            if (args.length) {
                H = function(e){
                    return h.apply(this,args);
                };
            }
            while (l--) {
                //hs.push([n,types[l],H]);
                n.addEventListener(types[l],H,false);
            }
        }

        function removeHandlers(pg) {
            /*var h, hs = handlers[pg[0]];
             if (hs) {
             while (h = hs.pop()) {
             h[0].removeEventListener(h[1],h[2]);
             }
             handlers[pg[0]] = [];
             }*/
        }

        function makeScrollable(n) {
            var els = n.querySelectorAll('.app-content'), l = els.length;
            while (l--) {
                if (!els[l].getAttribute('data-no-scroll')) {
                    Scrollable(els[l]);
                    addClass(els[l],'app-scrollable');
                }
            }
        }

        /*function addHandlers(pg) {
         var dom = pg[1];
         var hs = handlers[pg[0]] || (handlers[pg[0]] = []);
         makeButtons(dom,hs);
         addHandler(hs,dom,'click',function(e){
         var butt = getParent(e.target,'.app-button');
         if (butt) {
         var lData, bData = butt.getAttribute('data-back');
         if (bData) {
         back();
         } else if (lData = butt.getAttribute('data-target')) {
         loadPage(lData);
         }
         }
         });
         }*/
        function addHandlers() {
            if (handlersAdded) return;
            handlersAdded = true;
            makeButtons(d,{});
            addHandler({},d,'click',function(e){
                var butt = getParent(e.target,'.app-button');
                if (butt) {
                    var act = butt.getAttribute('data-action');
                    var cls = [];
                    arrayForEach.call(butt.className.split(' '),function(it){
                        buttClassActions[it] && cls.push(buttClassActions[it]);
                    });
                    var lData, bData = butt.getAttribute('data-back');
                    if (bData) {
                        back();
                    } else if (lData = butt.getAttribute('data-target')) {
                        loadPage(lData);
                    } else if (act && buttActions[(act = act.split(':'))[0]]) {
                        buttActions[act[0]](act[1].split(','));
                    } else if (cls.length) {
                        runFuncsArray(cls, butt, act, e.target);
                    }
                }
            });
        }
        function buildPageObject(pg) {
            return {
                name: pg[0],
                html: pg[1],
                context: pg[2],
                data: pg[3]
            }
        }
        function getPage(num) {
            if (!arguments.length) {
                num = history.length - 1;
            }
            if (isNaN(num)) {

            }
            else if (num<0) {
                num = history.length - num;
            }
            if (!history[num]) {

            } else {
                return buildPageObject(history[num]);
            }
            return false;
        }
        function createPage(pname,data) {
            var p;
            if (arguments.length==1) {
                p = pname;
                pname = p[0];
                data =  p[3];
            } else {
                if (!pages[pname] || !loads[pname]) {
                    return false;
                }
                data || (data = {});
                p = [pname,pages[pname].cloneNode(true),{},data];
            }
            loads[pname].call(p[2],p[1],data);
            return p;
        }
        function loadPage(pname,data,force) {
            if (pages[pname] && loads[pname] && (current[0]!=pname || force)) {
                var prev = current[0] && current;
                var next = [pname,pages[pname].cloneNode(true),{},data];
                if (!triggerEvent(next[1], 'page:load', {pageName: pname, pageContext: next[2], pageData: data, canStop: true})) return false;
                next[1].style.display = 'none';
                d.body && next[1].parentNode || d.body.appendChild(next[1]);
                createPage(next);
                history.push(current = next);
                triggerEvent(current[1], 'page:loaded', {pageName: current[0], pageData: data, pageContext: current[2]});
                switchPages(prev,current);
            }
        }
        function back() {
            var last = history[history.length-1];
            if (!triggerEvent(last[1], 'navigation:back', {
                root: history.length < 2,
                pageName: last[0],
                pageContext: last[2],
                pageData: last[3],
                canStop: true
            })) return true;
            if (history.length<2) {
                return false;
            }
            last = history.pop();
            current = history[history.length - 1];
            removeHandlers(last);
            switchPages(last,current);
            unloads[last[0]] && unloads[last[0]].call(last[2],last[1]);
            history.length < 2 && triggerEvent(current[1], 'page:root');
            return true;
        }
        function retryPage(data) {
            if (current[0]) {
                history.pop();
                removeHandlers(current);
                unloads[current[0]] && unloads[current[0]].call(current[2],current[1]);
                loadPage(current[0],data || current[3],true);
            }
        }
        function populatePage(pname, onload, onunload){
            loads[pname] = typeof onload == 'function'?  onload : loads[pname] || function(){};
            (typeof onunload == 'function') && (unloads[pname] = onunload);
        }

        /*function switchControl(cIdOff,cIdOn,useHistory) {
         var cOff = d.getElementById(cIdOff), cOn = d.getElementById(cIdOn);
         if (useHistory) {
         var hItem = [cOff,cOn];
         history.push(hItem);
         }
         }*/

        var util = {};

        util.device = {};
        util.device.os = (function() {
            var os = navigator.userAgent.toLowerCase();

            if (!!~os.indexOf('android')) {
                os = 'android';
            } else if (!!~os.indexOf('iphone') || !!~os.indexOf('ipad')) {
                os = 'ios';
            } else {
                os = 'default';
            }

            return os;
        })();
        util.device.version = (function(){
            var re = util.device.os + '\\s+([\\d\\.]+)';
            re = new RegExp(re);
            return (navigator.userAgent.toLowerCase().match(re, 'i') || [null, null])[1];
        })();
        util.device.platform = {};
        util.device.platform.isApp = (function(){
            return !!~navigator.userAgent.toLowerCase().indexOf('ICQ_WebApp');
        })();

        var config = {
            activeClass: function(ac) {
                if (typeof ac == 'undefined') {
                    return activeClass
                }
                return activeClass = ac;
            },
            transitionType: function(tr){
                if (typeof tr == 'undefined') {
                    return transition
                }
                if (typeof tr !== 'string' || !tr) {

                } else if (!transitions[tr]) {

                } else {
                    transitionFunc = transitions[transition = tr];
                    return tr;
                }
            }
        };

        transitionFunc = transitions[transition];

        d.body && addClass(d.body,'app-' + util.device.os);
        addHandlers();

        $(document.body).addClass('app-' + util.device.os);

        if (util.device.version) {
            $(document.body).addClass('app-' + util.device.os + '-' + parseInt(util.device.version));
            $(document.body).addClass('app-' + util.device.os + '-' + util.device.version.replace('.', '-'));
        }

        $(window).on('online', function(){
            $(document.body).removeClass('app-offline');
        });
        $(window).on('offline', function(){
            $(document.body).addClass('app-offline');
        });

        if (util.device.os === 'android') {
            $('.app-page-back-button .caption').text($('.app-page-title .caption').text());
        }

        return {
            util: util,
            config: config,
            populator: populatePage,
            populate: populatePage,
            back: function() {
                return back.apply(this,arguments);
            },
            load: function(pname,data){
                initPages();
                loadPage(pname,data);
            },
            retry: function(data){
                retryPage(data);
            },
            setDefaultTransition: function(tr){
                if (typeof transitions[tr] == 'function') transitionFunc = transitions[tr];
            },
            getPage: function(){
                return getPage.apply(this,arguments);
            },
            getPrevPage: function(){
                return getPage(-2);
            },
            getHistory: function() {
                var h = [], l = history.length;
                while (l--) {
                    h[l] = {name: history[l][0], data: history[l][2]};
                }
                return h;
            },
            spliceHistory: function(i,num,h){
                if (!history.length) return false;
                h = (h instanceof Array)? arSlice(h,0) : arSlice(arguments,2);
                var l = h.length, p, r = [];
                while (l--) {
                    (p = createPage(h[l].name,h[l].data)) && r.push(p);
                }
                p = history.pop();
                r = history.splice.apply(history,[i,num].concat(r));
                history.push(p);
                l = r.length;
                while (l--) {
                    removeHandlers(r[l]);
                    r[l] = {name: r[l][0], data: r[l][3]};
                }
                return r;
            },
            clearHistory: function(){
                var it = history.pop();
                while (it = history.pop()) {
                    removeHandlers(it);
                }
                history = it? [it] : [];
            },
            on: function(t,h,c,a){
                return bindHandler(t,h,c,a);
            },
            off: function(t,h,c,a){
                return unbindHandler(t,h,c,a);
            }
        }
    }

    window.App = new App();
})(window,document);
(function(e){function o(){try{return r in e&&e[r]}catch(t){return!1}}var t={},n=e.document,r="localStorage",i="script",s;t.disabled=!1,t.set=function(e,t){},t.get=function(e){},t.remove=function(e){},t.clear=function(){},t.transact=function(e,n,r){var i=t.get(e);r==null&&(r=n,n=null),typeof i=="undefined"&&(i=n||{}),r(i),t.set(e,i)},t.getAll=function(){},t.forEach=function(){},t.serialize=function(e){return JSON.stringify(e)},t.deserialize=function(e){if(typeof e!="string")return undefined;try{return JSON.parse(e)}catch(t){return e||undefined}};if(o())s=e[r],t.set=function(e,n){return n===undefined?t.remove(e):(s.setItem(e,t.serialize(n)),n)},t.get=function(e){return t.deserialize(s.getItem(e))},t.remove=function(e){s.removeItem(e)},t.clear=function(){s.clear()},t.getAll=function(){var e={};return t.forEach(function(t,n){e[t]=n}),e},t.forEach=function(e){for(var n=0;n<s.length;n++){var r=s.key(n);e(r,t.get(r))}};else if(n.documentElement.addBehavior){var u,a;try{a=new ActiveXObject("htmlfile"),a.open(),a.write("<"+i+">document.w=window</"+i+'><iframe src="/favicon.ico"></iframe>'),a.close(),u=a.w.frames[0].document,s=u.createElement("div")}catch(f){s=n.createElement("div"),u=n.body}function l(e){return function(){var n=Array.prototype.slice.call(arguments,0);n.unshift(s),u.appendChild(s),s.addBehavior("#default#userData"),s.load(r);var i=e.apply(t,n);return u.removeChild(s),i}}var c=new RegExp("[!\"#$%&'()*+,/\\\\:;<=>?@[\\]^`{|}~]","g");function h(e){return e.replace(/^d/,"___$&").replace(c,"___")}t.set=l(function(e,n,i){return n=h(n),i===undefined?t.remove(n):(e.setAttribute(n,t.serialize(i)),e.save(r),i)}),t.get=l(function(e,n){return n=h(n),t.deserialize(e.getAttribute(n))}),t.remove=l(function(e,t){t=h(t),e.removeAttribute(t),e.save(r)}),t.clear=l(function(e){var t=e.XMLDocument.documentElement.attributes;e.load(r);for(var n=0,i;i=t[n];n++)e.removeAttribute(i.name);e.save(r)}),t.getAll=function(e){var n={};return t.forEach(function(e,t){n[e]=t}),n},t.forEach=l(function(e,n){var r=e.XMLDocument.documentElement.attributes;for(var i=0,s;s=r[i];++i)n(s.name,t.deserialize(e.getAttribute(s.name)))})}try{var p="__storejs__";t.set(p,p),t.get(p)!=p&&(t.disabled=!0),t.remove(p)}catch(f){t.disabled=!0}t.enabled=!t.disabled,typeof module!="undefined"&&module.exports&&this.module!==module?module.exports=t:typeof define=="function"&&define.amd?define(t):e.store=t})(Function("return this")());
/*!

 handlebars v2.0.0-alpha.2

 Copyright (C) 2011-2014 by Yehuda Katz

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

 @license
 */
/* exported Handlebars */
this.Handlebars = (function() {
// handlebars/safe-string.js
    var __module3__ = (function() {
        "use strict";
        var __exports__;
        // Build out our basic SafeString type
        function SafeString(string) {
            this.string = string;
        }

        SafeString.prototype.toString = function() {
            return "" + this.string;
        };

        __exports__ = SafeString;
        return __exports__;
    })();

// handlebars/utils.js
    var __module2__ = (function(__dependency1__) {
        "use strict";
        var __exports__ = {};
        /*jshint -W004 */
        var SafeString = __dependency1__;

        var escape = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#x27;",
            "`": "&#x60;"
        };

        var badChars = /[&<>"'`]/g;
        var possible = /[&<>"'`]/;

        function escapeChar(chr) {
            return escape[chr] || "&amp;";
        }

        function extend(obj /* , ...source */) {
            for (var i = 1; i < arguments.length; i++) {
                for (var key in arguments[i]) {
                    if (Object.prototype.hasOwnProperty.call(arguments[i], key)) {
                        obj[key] = arguments[i][key];
                    }
                }
            }

            return obj;
        }

        __exports__.extend = extend;var toString = Object.prototype.toString;
        __exports__.toString = toString;
        // Sourced from lodash
        // https://github.com/bestiejs/lodash/blob/master/LICENSE.txt
        var isFunction = function(value) {
            return typeof value === 'function';
        };
        // fallback for older versions of Chrome and Safari
        if (isFunction(/x/)) {
            isFunction = function(value) {
                return typeof value === 'function' && toString.call(value) === '[object Function]';
            };
        }
        var isFunction;
        __exports__.isFunction = isFunction;
        var isArray = Array.isArray || function(value) {
            return (value && typeof value === 'object') ? toString.call(value) === '[object Array]' : false;
        };
        __exports__.isArray = isArray;

        function escapeExpression(string) {
            // don't escape SafeStrings, since they're already safe
            if (string instanceof SafeString) {
                return string.toString();
            } else if (!string && string !== 0) {
                return "";
            }

            // Force a string conversion as this will be done by the append regardless and
            // the regex test will do this transparently behind the scenes, causing issues if
            // an object's to string has escaped characters in it.
            string = "" + string;

            if(!possible.test(string)) { return string; }
            return string.replace(badChars, escapeChar);
        }

        __exports__.escapeExpression = escapeExpression;function isEmpty(value) {
            if (!value && value !== 0) {
                return true;
            } else if (isArray(value) && value.length === 0) {
                return true;
            } else {
                return false;
            }
        }

        __exports__.isEmpty = isEmpty;function appendContextPath(contextPath, id) {
            return (contextPath ? contextPath + '.' : '') + id;
        }

        __exports__.appendContextPath = appendContextPath;
        return __exports__;
    })(__module3__);

// handlebars/exception.js
    var __module4__ = (function() {
        "use strict";
        var __exports__;

        var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];

        function Exception(message, node) {
            var line;
            if (node && node.firstLine) {
                line = node.firstLine;

                message += ' - ' + line + ':' + node.firstColumn;
            }

            var tmp = Error.prototype.constructor.call(this, message);

            // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.
            for (var idx = 0; idx < errorProps.length; idx++) {
                this[errorProps[idx]] = tmp[errorProps[idx]];
            }

            if (line) {
                this.lineNumber = line;
                this.column = node.firstColumn;
            }
        }

        Exception.prototype = new Error();

        __exports__ = Exception;
        return __exports__;
    })();

// handlebars/base.js
    var __module1__ = (function(__dependency1__, __dependency2__) {
        "use strict";
        var __exports__ = {};
        var Utils = __dependency1__;
        var Exception = __dependency2__;

        var VERSION = "2.0.0-alpha.2";
        __exports__.VERSION = VERSION;var COMPILER_REVISION = 5;
        __exports__.COMPILER_REVISION = COMPILER_REVISION;
        var REVISION_CHANGES = {
            1: '<= 1.0.rc.2', // 1.0.rc.2 is actually rev2 but doesn't report it
            2: '== 1.0.0-rc.3',
            3: '== 1.0.0-rc.4',
            4: '== 1.x.x',
            5: '>= 2.0.0'
        };
        __exports__.REVISION_CHANGES = REVISION_CHANGES;
        var isArray = Utils.isArray,
            isFunction = Utils.isFunction,
            toString = Utils.toString,
            objectType = '[object Object]';

        function HandlebarsEnvironment(helpers, partials) {
            this.helpers = helpers || {};
            this.partials = partials || {};

            registerDefaultHelpers(this);
        }

        __exports__.HandlebarsEnvironment = HandlebarsEnvironment;HandlebarsEnvironment.prototype = {
            constructor: HandlebarsEnvironment,

            logger: logger,
            log: log,

            registerHelper: function(name, fn, inverse) {
                if (toString.call(name) === objectType) {
                    if (inverse || fn) { throw new Exception('Arg not supported with multiple helpers'); }
                    Utils.extend(this.helpers, name);
                } else {
                    if (inverse) { fn.not = inverse; }
                    this.helpers[name] = fn;
                }
            },
            unregisterHelper: function(name) {
                delete this.helpers[name];
            },

            registerPartial: function(name, str) {
                if (toString.call(name) === objectType) {
                    Utils.extend(this.partials,  name);
                } else {
                    this.partials[name] = str;
                }
            },
            unregisterPartial: function(name) {
                delete this.partials[name];
            }
        };

        function registerDefaultHelpers(instance) {
            instance.registerHelper('helperMissing', function(/* [args, ]options */) {
                if(arguments.length === 1) {
                    // A missing field in a {{foo}} constuct.
                    return undefined;
                } else {
                    // Someone is actually trying to call something, blow up.
                    throw new Exception("Missing helper: '" + arguments[arguments.length-1].name + "'");
                }
            });

            instance.registerHelper('blockHelperMissing', function(context, options) {
                var inverse = options.inverse || function() {}, fn = options.fn;

                if (isFunction(context)) { context = context.call(this); }

                if(context === true) {
                    return fn(this);
                } else if(context === false || context == null) {
                    return inverse(this);
                } else if (isArray(context)) {
                    if(context.length > 0) {
                        if (options.ids) {
                            options.ids = [options.name];
                        }

                        return instance.helpers.each(context, options);
                    } else {
                        return inverse(this);
                    }
                } else {
                    if (options.data && options.ids) {
                        var data = createFrame(options.data);
                        data.contextPath = Utils.appendContextPath(options.data.contextPath, options.name);
                        options = {data: data};
                    }

                    return fn(context, options);
                }
            });

            instance.registerHelper('each', function(context, options) {
                // Allow for {{#each}}
                if (!options) {
                    options = context;
                    context = this;
                }

                var fn = options.fn, inverse = options.inverse;
                var i = 0, ret = "", data;

                var contextPath;
                if (options.data && options.ids) {
                    contextPath = Utils.appendContextPath(options.data.contextPath, options.ids[0]) + '.';
                }

                if (isFunction(context)) { context = context.call(this); }

                if (options.data) {
                    data = createFrame(options.data);
                }

                if(context && typeof context === 'object') {
                    if (isArray(context)) {
                        for(var j = context.length; i<j; i++) {
                            if (data) {
                                data.index = i;
                                data.first = (i === 0);
                                data.last  = (i === (context.length-1));

                                if (contextPath) {
                                    data.contextPath = contextPath + i;
                                }
                            }
                            ret = ret + fn(context[i], { data: data });
                        }
                    } else {
                        for(var key in context) {
                            if(context.hasOwnProperty(key)) {
                                if(data) {
                                    data.key = key;
                                    data.index = i;
                                    data.first = (i === 0);

                                    if (contextPath) {
                                        data.contextPath = contextPath + key;
                                    }
                                }
                                ret = ret + fn(context[key], {data: data});
                                i++;
                            }
                        }
                    }
                }

                if(i === 0){
                    ret = inverse(this);
                }

                return ret;
            });

            instance.registerHelper('if', function(conditional, options) {
                if (isFunction(conditional)) { conditional = conditional.call(this); }

                // Default behavior is to render the positive path if the value is truthy and not empty.
                // The `includeZero` option may be set to treat the condtional as purely not empty based on the
                // behavior of isEmpty. Effectively this determines if 0 is handled by the positive path or negative.
                if ((!options.hash.includeZero && !conditional) || Utils.isEmpty(conditional)) {
                    return options.inverse(this);
                } else {
                    return options.fn(this);
                }
            });

            instance.registerHelper('unless', function(conditional, options) {
                return instance.helpers['if'].call(this, conditional, {fn: options.inverse, inverse: options.fn, hash: options.hash});
            });

            instance.registerHelper('with', function(context, options) {
                if (isFunction(context)) { context = context.call(this); }

                var fn = options.fn;

                if (!Utils.isEmpty(context)) {
                    if (options.data && options.ids) {
                        var data = createFrame(options.data);
                        data.contextPath = Utils.appendContextPath(options.data.contextPath, options.ids[0]);
                        options = {data:data};
                    }

                    return fn(context, options);
                }
            });

            instance.registerHelper('log', function(context, options) {
                var level = options.data && options.data.level != null ? parseInt(options.data.level, 10) : 1;
                instance.log(level, context);
            });

            instance.registerHelper('lookup', function(obj, field, options) {
                return obj && obj[field];
            });
        }

        var logger = {
            methodMap: { 0: 'debug', 1: 'info', 2: 'warn', 3: 'error' },

            // State enum
            DEBUG: 0,
            INFO: 1,
            WARN: 2,
            ERROR: 3,
            level: 3,

            // can be overridden in the host environment
            log: function(level, obj) {
                if (logger.level <= level) {
                    var method = logger.methodMap[level];
                    if (typeof console !== 'undefined' && console[method]) {
                        console[method].call(console, obj);
                    }
                }
            }
        };
        __exports__.logger = logger;
        function log(level, obj) { logger.log(level, obj); }

        __exports__.log = log;var createFrame = function(object) {
            var frame = Utils.extend({}, object);
            frame._parent = object;
            return frame;
        };
        __exports__.createFrame = createFrame;
        return __exports__;
    })(__module2__, __module4__);

// handlebars/runtime.js
    var __module5__ = (function(__dependency1__, __dependency2__, __dependency3__) {
        "use strict";
        var __exports__ = {};
        var Utils = __dependency1__;
        var Exception = __dependency2__;
        var COMPILER_REVISION = __dependency3__.COMPILER_REVISION;
        var REVISION_CHANGES = __dependency3__.REVISION_CHANGES;
        var createFrame = __dependency3__.createFrame;

        function checkRevision(compilerInfo) {
            var compilerRevision = compilerInfo && compilerInfo[0] || 1,
                currentRevision = COMPILER_REVISION;

            if (compilerRevision !== currentRevision) {
                if (compilerRevision < currentRevision) {
                    var runtimeVersions = REVISION_CHANGES[currentRevision],
                        compilerVersions = REVISION_CHANGES[compilerRevision];
                    throw new Exception("Template was precompiled with an older version of Handlebars than the current runtime. "+
                        "Please update your precompiler to a newer version ("+runtimeVersions+") or downgrade your runtime to an older version ("+compilerVersions+").");
                } else {
                    // Use the embedded version info since the runtime doesn't know about this revision yet
                    throw new Exception("Template was precompiled with a newer version of Handlebars than the current runtime. "+
                        "Please update your runtime to a newer version ("+compilerInfo[1]+").");
                }
            }
        }

        __exports__.checkRevision = checkRevision;// TODO: Remove this line and break up compilePartial

        function template(templateSpec, env) {
            if (!env) {
                throw new Exception("No environment passed to template");
            }

            // Note: Using env.VM references rather than local var references throughout this section to allow
            // for external users to override these as psuedo-supported APIs.
            env.VM.checkRevision(templateSpec.compiler);

            var invokePartialWrapper = function(partial, name, context, hash, helpers, partials, data) {
                if (hash) {
                    context = Utils.extend({}, context, hash);
                }

                var result = env.VM.invokePartial.call(this, partial, name, context, helpers, partials, data);
                if (result != null) { return result; }

                if (env.compile) {
                    var options = { helpers: helpers, partials: partials, data: data };
                    partials[name] = env.compile(partial, { data: data !== undefined }, env);
                    return partials[name](context, options);
                } else {
                    throw new Exception("The partial " + name + " could not be compiled when running in runtime-only mode");
                }
            };

            // Just add water
            var container = {
                escapeExpression: Utils.escapeExpression,
                invokePartial: invokePartialWrapper,

                fn: function(i) {
                    return templateSpec[i];
                },

                programs: [],
                program: function(i, data) {
                    var programWrapper = this.programs[i],
                        fn = this.fn(i);
                    if(data) {
                        programWrapper = program(this, i, fn, data);
                    } else if (!programWrapper) {
                        programWrapper = this.programs[i] = program(this, i, fn);
                    }
                    return programWrapper;
                },
                programWithDepth: env.VM.programWithDepth,

                data: function(data, depth) {
                    while (data && depth--) {
                        data = data._parent;
                    }
                    return data;
                },
                merge: function(param, common) {
                    var ret = param || common;

                    if (param && common && (param !== common)) {
                        ret = Utils.extend({}, common, param);
                    }

                    return ret;
                },

                noop: env.VM.noop,
                compilerInfo: templateSpec.compiler
            };

            var ret = function(context, options) {
                options = options || {};
                var helpers,
                    partials,
                    data = options.data;

                ret._setup(options);
                if (!options.partial && templateSpec.useData) {
                    data = initData(context, data);
                }
                return templateSpec.main.call(container, context, container.helpers, container.partials, data);
            };

            ret._setup = function(options) {
                if (!options.partial) {
                    container.helpers = container.merge(options.helpers, env.helpers);

                    if (templateSpec.usePartial) {
                        container.partials = container.merge(options.partials, env.partials);
                    }
                } else {
                    container.helpers = options.helpers;
                    container.partials = options.partials;
                }
            };

            ret._child = function(i) {
                return container.programWithDepth(i);
            };
            return ret;
        }

        __exports__.template = template;function programWithDepth(i, data /*, $depth */) {
            /*jshint -W040 */
            var args = Array.prototype.slice.call(arguments, 2),
                container = this,
                fn = container.fn(i);

            var prog = function(context, options) {
                options = options || {};

                return fn.apply(container, [context, container.helpers, container.partials, options.data || data].concat(args));
            };
            prog.program = i;
            prog.depth = args.length;
            return prog;
        }

        __exports__.programWithDepth = programWithDepth;function program(container, i, fn, data) {
            var prog = function(context, options) {
                options = options || {};

                return fn.call(container, context, container.helpers, container.partials, options.data || data);
            };
            prog.program = i;
            prog.depth = 0;
            return prog;
        }

        __exports__.program = program;function invokePartial(partial, name, context, helpers, partials, data) {
            var options = { partial: true, helpers: helpers, partials: partials, data: data };

            if(partial === undefined) {
                throw new Exception("The partial " + name + " could not be found");
            } else if(partial instanceof Function) {
                return partial(context, options);
            }
        }

        __exports__.invokePartial = invokePartial;function noop() { return ""; }

        __exports__.noop = noop;function initData(context, data) {
            if (!data || !('root' in data)) {
                data = data ? createFrame(data) : {};
                data.root = context;
            }
            return data;
        }
        return __exports__;
    })(__module2__, __module4__, __module1__);

// handlebars.runtime.js
    var __module0__ = (function(__dependency1__, __dependency2__, __dependency3__, __dependency4__, __dependency5__) {
        "use strict";
        var __exports__;
        /*globals Handlebars: true */
        var base = __dependency1__;

        // Each of these augment the Handlebars object. No need to setup here.
        // (This is done to easily share code between commonjs and browse envs)
        var SafeString = __dependency2__;
        var Exception = __dependency3__;
        var Utils = __dependency4__;
        var runtime = __dependency5__;

        // For compatibility and usage outside of module systems, make the Handlebars object a namespace
        var create = function() {
            var hb = new base.HandlebarsEnvironment();

            Utils.extend(hb, base);
            hb.SafeString = SafeString;
            hb.Exception = Exception;
            hb.Utils = Utils;

            hb.VM = runtime;
            hb.template = function(spec) {
                return runtime.template(spec, hb);
            };

            return hb;
        };

        var Handlebars = create();
        Handlebars.create = create;

        __exports__ = Handlebars;
        return __exports__;
    })(__module1__, __module3__, __module4__, __module2__, __module5__);

    return __module0__;
})();
