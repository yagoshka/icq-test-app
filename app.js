var EXPRESS = require('express');
    app = EXPRESS(),
    compression = require('compression'),
    config = require('./config');

app.use(compression());

app.use(function(err, req, res, next){
    console.error(err);
    res.send(500);
});

app.use('/js', EXPRESS.static(__dirname + '/' + config.staticDir + '/js'));
app.use('/css', EXPRESS.static(__dirname + '/' + config.staticDir + '/css'));
app.use('/images', EXPRESS.static(__dirname + '/' + config.staticDir + '/images'));

app.get('/', function(req, res) {
    res.sendfile(config.staticDir + '/index.html');
});

app.get('/manifest.appcache', function(req, res) {
    res.type('text/cache-manifest');
    res.sendfile(config.staticDir + '/manifest.appcache');
});

app.listen(config.port);