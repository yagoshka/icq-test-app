/*global module:false*/

var defaultJSHintOptions = {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        unused: false,
        boss: true,
        eqnull: true,
        browser: true,
        laxbreak: true,
        laxcomma: true,
        evil: true,
        ignores: ['static/**/lib/**/*.js', 'static/**/lib.js', 'static/**/templates.js']
    },
    defaultJSHintGlobals = {
        jQuery: true,
        Handlebars: true,
        App: true,
        Youtube: true,
        $: true,
        YT: true,
        store: true,
        mailru: true,
        ENV: true,
        alert: true,
        unescape: true
    };


module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        // Metadata.
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
            '<%= grunt.template.today("yyyy-mm-dd") %>\n */',
        // Task configuration.
        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: true
            },
            app: {
                src: ['static/dev/js/templates.js', 'static/dev/js/template_helpers.js', 'static/dev/js/*.js', '!static/dev/js/all.js', '!static/dev/js/lib.js'],
                dest: 'static/dev/js/all.js'
            },
            lib: {
                src: ['static/dev/js/lib/app/zepto.js', 'static/dev/js/lib/**/*.js'],
                dest: 'static/dev/js/lib.js'
            }
        },
        uglify: {
            options: {
                banner: '<%= banner %>'
            },
            dist: {
                cwd: 'static/dev/js',
                src: ['all.js', 'lib.js'],
                expand: true,
                ext: ".js",
                dest: 'static/release/js/'
            }
        },
        jshint: {
            options: defaultJSHintOptions,
            dev: {
                options: grunt.util._.extend(
                    {
                        globals: grunt.util._.extend({
                            console: true,
                            $: true
                        }, defaultJSHintGlobals)
                    }, defaultJSHintOptions
                ),
                src: ['static/dev/**/*.js']
            },
            dist: {
                options: grunt.util._.extend(
                    {
                        globals: defaultJSHintGlobals
                    }, defaultJSHintOptions
                ),
                src: ['static/dev/**/*.js']
            },
            gruntfile: {
                src: 'Gruntfile.js'
            }
        },
        watch: {
            gruntfile: {
                files: '<%= jshint.gruntfile.src %>',
                tasks: ['jshint:gruntfile']
            },
            test: {
                files: '<%= jshint.dev.src %>',
                tasks: ['jshint:dev']
            },
            compass: {
                files: ['static/dev/sass/*.sass'],
                tasks: ['compass:dev']
            },
            handlebars: {
                files: ['templates/**/*.handlebars'],
                tasks: ['handlebars:dev']
            },
            'compile-handlebars': {
                files: ['templates/**/*.handlebars'],
                tasks: ['compile-handlebars:dev'],
                options: {
                    debounceDelay: 500
                }
            },
            concat: {
                files: ['static/dev/js/**/*.js', '!static/dev/js/lib.js','!static/dev/js/all.js'],
                tasks: 'concat'
            },
            manifest: {
                files: ['static/dev/js/lib.js','!static/dev/js/all.js'],
                tasks: 'manifest'
            }
        },
        compass: {
            options: {
                sassDir: 'static/dev/sass',
                outputStyle: 'compressed'
            },
            dist: {
                options: {
                    cssDir: 'static/release/css',
                    force: true
                }
            },
            dev: {
                options: {
                    cssDir: 'static/dev/css'
                }
            }

        },
        handlebars: {
            dist: {},
            dev: {
                destination: 'static/dev/js/templates.js',
                options: {

                },
                files: {
                    '<%= handlebars.dev.destination %>': ['templates/**/*.handlebars']
                }
            }
        },
        'compile-handlebars': {
            dev: {
                template: 'templates/layout.handlebars',
                output: 'static/dev/index.html',
                partials: 'templates/**/*.handlebars',
                templateData: 'data.json'
            },
            dist: {
                template: 'templates/layout.handlebars',
                output: 'static/release/index.html',
                partials: 'templates/**/*.handlebars',
                templateData: 'data.json'
            }
        },

        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: {
                    'static/release/index.html': 'static/dev/index.html'
                }
            }
        },
        manifest: {
            generate: {
                options: {
                    cache: ['/js/all.js', '/js/lib.js', '/css/app/app.css', '/css/main.css', '/images/play.png', '/images/black_preloader.gif']
                },
                src: ['/static/dev/js/all.js', '/static/dev/js/lib.js', '/static/dev/css/*.css', '/static/dev/images/*.*'],
                dest: 'static/release/manifest.appcache'
            }
        }

    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.loadNpmTasks('grunt-compile-handlebars');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-manifest');

    // Default task.
    grunt.registerTask('default', ['jshint:gruntfile', 'handlebars:dev', 'compass:dev', 'concat', 'uglify', 'compile-handlebars:dev', 'manifest']);
    grunt.registerTask('release', ['handlebars', 'jshint:dist', 'concat', 'uglify', 'compass', 'compile-handlebars:dist', 'htmlmin:dist', 'manifest']);
};