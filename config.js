var extend = require('extend'),
    config = {};

config.env = process.env.NODE_ENV || 'dev';

console.log(config.env);

switch (config.env) {
    case 'prod':
        extend(config, require('./config.prod.js'));
        break;

    default:
        extend(config,  require('./config.dev.js'));
}

module.exports = config;